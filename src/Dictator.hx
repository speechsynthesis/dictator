package;


#if cpp
import cpp.NativeSys;
#end
import sys.FileSystem;
import sys.io.File;
#if python
import pyatspi.registry.Registry;
import python.Syntax;
import python.lib.Threading;
#end
#if (target.threaded)
import sys.thread.Thread;
#elseif python
import python.lib.threading.Thread;
#end

using StringTools;


@:pythonImport("os")
extern class Os2 {
	static function getpid():Int;
}
/**
	For threading in python
**/
@:pythonImport("queue", "Queue")
extern class Queue {
	@:native("__new__")
	function new();

	function get():Dynamic;
	function put(item:Dynamic):Void;
}

@:buildXml("<target  id='haxe' tool='linker' ><flag value='-Wl,--disable-new-dtags,-rpath,$ORIGIN' /><flag value='-Wl,--disable-new-dtags,-rpath,$ORIGIN/libs' /></target>")
class Dictator {

	public static var PID_FILE = "/tmp/dictator_pid";
	public static var commandType = null;

	static var listensForCommand = true;

	public static var commandTool:CommandTool = new CommandTool.XDOTool();

	public static var mode:Mode;

	public static var locale:Locale;

	public var showHints:Bool = true;

	public static var thread:Thread;
	#if python public static var queue:Dynamic; #end

	public static function main() {


		// Check if instance is already running
		// Ideally should also use other techniques https://stackoverflow.com/questions/220525/ensure-a-single-instance-of-an-application-in-linux
		var nowPid:String = #if cpp "" + NativeSys.sys_get_pid()
		             #elseif python ("" + Os2.getpid())
					 #else "" #end;
		if (FileSystem.exists(PID_FILE)) {		
			var previous_pid = File.getContent(PID_FILE);
			
			if (new sys.io.Process('ps -p $previous_pid -o comm=').stdout.readAll().toString() == new sys.io.Process('ps -p $nowPid -o comm=').stdout.readAll().toString()) {
				trace("Dictator instance is already running");
				Sys.exit(1);
			}
			else {
				File.saveContent(PID_FILE, "" + nowPid);
			}
		}
		else {
			File.saveContent(PID_FILE, nowPid);
		}

		if (Sys.args().length == 0) {
			Sys.println("Don't forget to add a parameter");
			return;
		}
		#if (target.threaded)
		thread = Thread.current();
		STT.returnThread = Thread.current();
		#elseif python
		thread = Threading.current_thread();
		STT.returnThread = thread;
		#end
		locale = new Locale();
		locale.load(Sys.args()[0]);

		var modelPath = Util.absolutePathForFile(locale.modelPath);

		function whenRecognizesPartialResults(json:Dynamic) {
			var o = haxe.Json.parse(json);

			if (o.partial == "")
				return;

			mode = mode.listensForCommand(o.partial, true, [o.partial]);
		}

		function whenRecognizesResults(ob:Dynamic) {
			var o = haxe.Json.parse(ob.json);
			var alts:Array<Dynamic> = o.alternatives;
			if (alts.length < 1)
				return;
			var t:String = alts[0].text;
			if (t == "")
				return;
			trace(alts);
			mode = mode.listensForCommand(t.trim(), false, alts);
		}

		STT.onModelLoaded = function() {
			mode = new Mode.BasicMode();
			STT.loadRec();
			STT.startsListening();
		};

		#if (target.threaded)
		sys.thread.Thread.create(() -> {
			Helper.showText("Loading Model ...");
			STT.loadModel(modelPath);
		});
		#elseif python
		queue = new Queue();

		final t = new python.lib.threading.Thread({
			target: () -> {
				Helper.showText("Loading Model ...");
				try {
				STT.loadModel(modelPath);
				}
				catch(e) {
					trace(e);
					Sys.exit(1);
				}
			}
		});
		t.daemon = true;
		t.start();
		#end

		#if python
		final t = new python.lib.threading.Thread({
			target: () -> {
				Atspi.initialize();
			}
		});
		t.daemon = true;
		t.start();
		#end

		while (true) {
			#if (target.threaded)
			var a = Thread.readMessage(true);
			#elseif python
			var a = queue.get();
			#end
			var isString = Std.isOfType(a, String);
			if (isString) {
				whenRecognizesPartialResults(a);
			} else {
				whenRecognizesResults(a);
			}
		}
	}
}
