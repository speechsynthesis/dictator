using StringTools;

interface Grammar { 
	public function correctGrammar(text:String, dictation:Mode.DictationMode):Void;
}

class Grammalecte implements Grammar {

	public function new() {
	}


	public function correctGrammar(text:String, dictation:Mode.DictationMode) {
		var request = new haxe.Http("http://localhost:8080/gc_text/fr");

		var post = {
			"text": text,
			"tf": true,
			"options": {}
		}

		request.addParameter("text", text);
		request.addHeader("Content-Type", "application/x-www-form-urlencoded");

		request.onData = function(dataF) {
			var dataJSON = haxe.Json.parse(dataF);
			var newText = text;

			var data:Array<Dynamic> = cast dataJSON.data;
			var offset = 0;
			for (d in data) {
				for (g in(cast d.lGrammarErrors : Array<Dynamic>)) {
					g.nStart += offset;
					g.nEnd += offset;
					var diff = g.aSuggestions[0] - (g.nEnd - g.nStart);

					offset += Std.int(diff);
					newText = Mode.DictationMode.replacePart(newText, g.nStart, g.nEnd, g.aSuggestions[0]);
				}
				trace("correct:"+newText+"__");
				dictation.type(text, false);
			}
		}

		request.onError = function(data) {
			trace('error with grammalecte');
			dictation.type(text, false);
		}

		request.request(true); // true pour un POST
	}

}