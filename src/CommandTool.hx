import sys.io.Process;

interface CommandTool {
	public function key(key:String, delayMS:Float):Void;
	public function type(typed:String, delayMS:Float):Process;
}

class XDOTool implements CommandTool {
	public function new() {}

	public function key(key:String, delayMS:Float) {
		var params = ['key', '--clearmodifiers', '--delay', '$delayMS', key];

		if (Context.windowId != null) {
			params.insert(1, "--window" );
			params.insert(2, Context.windowId);
		}
		new sys.io.Process("xdotool", params);
	}

	public function type(typed:String, delayMS:Float) {
		var params = ['type', '--clearmodifiers', '--delay', '$delayMS', typed];

		if (Context.windowId != null) {
			params.insert(1, "--window" );
			params.insert(2, Context.windowId);
		}
		return new sys.io.Process("xdotool", params );
	}
}

class YDOTool implements CommandTool {
	private var map:Map<Int, Array<String>> = new Map();

	public function new() {
		var p = new sys.io.Process("dumpkeys -n");
		var lines = p.stdout.readAll().toString();
		for (l in lines.split("\n")) {
			var i = l.indexOf("=");
			var letter = l.substr(i + 2);
			letter = StringTools.trim(letter);
			if (StringTools.startsWith(letter, "+")) {
				letter = letter.substr(1);
				var letterC = Std.parseInt(letter) - 0x0b00; // .charCodeAt(0);
				if (!map.exists(letterC)) {
					var coms = StringTools.trim(l.substr(0, i)).split("\t");
					map[letterC] = coms;
				}
			} else {
				var letterC = Std.parseInt(letter);
				if (!map.exists(letterC)) {
					var coms = StringTools.trim(l.substr(0, i)).split("\t");
					map[letterC] = coms;
				}
				continue;
			}
		}
		trace(map);
	}

	public function key(key:String, delayMS:Float) {
		var keyCode = switch (key) {
			case "Backspace": "14";
			case _: key;
		}
		new sys.io.Process("ydotool key " + keyCode + " -d " + delayMS * 1000);
	}

	private function keyCodes(key:String, delayMS:Float) {
		return new sys.io.Process("ydotool key " + key + " -d " + delayMS);
	}

	public function type(type:String, delayMS:Float) {
		var keys = "";
		for (k in type.split("")) {
			var keyCode = k.charCodeAt(0);

			for (a in map[keyCode]) {
				if (a.indexOf("keycode") != -1) {
					var keycode = StringTools.replace(a, "keycode", "");
					keycode = StringTools.trim(keycode);
					var c = ' $keycode:1 $keycode:0';
					keys += c;
				}
			}
		}
		return keyCodes(keys, 1);
	}
}