
#if (target.threaded)
import sys.thread.Thread;
#elseif python
import python.lib.threading.Thread;
#end

class STT {
	public static var isListening = false;

	static var record:Null<sys.io.Process> = null;

	public static var whenRecognizesPartialResults:Dynamic->Void = null;

	public static var whenRecognizesResults:Dynamic->Void = null;

	public static var doRecord = false;

	public static var onModelLoaded:Void->Void = null;

	static var rec:vosk.Vosk.VoskRecognizer = null;

	static var model:vosk.Vosk.VoskModel = null;

	static var doForceRecognition:Bool = false;

	public static var returnThread:Thread;

    public static var isModelLoaded = false;

	public static function loadModel(path:String) {
		#if cpp cpp.vm.Gc.enterGCFreeZone(); #end
		model = vosk.Vosk.voskModelNew(path);
		#if cpp cpp.vm.Gc.exitGCFreeZone(); #end
        if (model ==  null) {
			Helper.remove();
			Sys.exit(1);
			return;
		}
		onModelLoaded();
        isModelLoaded = true;
	}

    public static function checkWordPresence(s:String) {
        return vosk.Vosk.voskModelFindWord(model, s.toLowerCase());
    }

	public static function loadRec(words:Array<String> = null) {
		if (words == null) {
			rec = vosk.Vosk.voskRecognizerNew(model, 16000);
		} else {
			words.push("[unk]");
			for (w in 0...words.length) {
				words[w] = "\"" + words[w] + "\"";
			}
			var l = "" + words;

			trace(l);

			rec = vosk.Vosk.voskRecognizerNewGrm(model, 16000, l);
		}
		vosk.Vosk.voskRecognizerSetMaxAlternatives(rec, 10);
		vosk.Vosk.voskRecognizerSetWords(rec, 10);
		vosk.Vosk.voskRecognizerSetPartialWords(rec, 10);
	}

	public static function startsListening() {
		if (isListening)
			return;
		isListening = true;

		record = new sys.io.Process("arecord", ["-c", "1", "-f", "S16_LE", "-r", "16000"]);

		Sys.sleep(1);

		var buf = new haxe.io.BytesBuffer();
		var recording = new haxe.io.BytesBuffer();
		var totalBytesSend = 0;
		var totalBytesSendFromFinal = 0;

		while (isListening) {
			if (record == null)
				continue;
			try {
                var buff = haxe.io.Bytes.alloc(16000);
                var len = record.stdout.readBytes(buff, 0, 16000);
                buf.addBytes(buff, 0, len);

				if (buf.length >= 8000) {
					totalBytesSend += buf.length;
					var byt = buf.getBytes();
					buf = new haxe.io.BytesBuffer();
					recording.add(byt);
					var a #if cpp :cpp.ArrayBase #end = cast byt.getData();
					var wavAccept = vosk.Vosk.voskRecognizerAcceptWaveform(rec, #if cpp cast a.getBase() #else a #end, byt.length);
					if ((wavAccept == 1) || doForceRecognition) {
						var fin = vosk.Vosk.voskRecognizerResult(rec);

						var startMs = Std.int(totalBytesSendFromFinal / (16000 * 1 * 16 / 8) * 1000);
							var obj = {json: fin, recording: recording.getBytes(), startMs: startMs};
							#if (target.threaded) returnThread.sendMessage(obj); 
							#elseif python Dictator.queue.put(obj);
							#end
						recording = new haxe.io.BytesBuffer();
						totalBytesSendFromFinal = totalBytesSend;
						doForceRecognition = false;
					} else if (wavAccept == 0) {
						var fin = vosk.Vosk.voskRecognizerPartialResult(rec);
						#if (target.threaded) returnThread.sendMessage(fin); #end
					} else {
						trace("error");
					}
				}
			} catch (e) {
				trace(e);
			}
		}
	}

	public static function reset() {
		vosk.Vosk.voskRecognizerReset(rec);
	}

	public static function forceRecognition() {
		doForceRecognition = true;
	}

	public static function stopsListening() {
		record.close();

		isListening = false;
	}
}
