import pyatspi.Action;
import pyatspi.text.Text;
import pyatspi.accessibility.Accessibility_Module;
import python.Syntax;
import pyatspi.accessibility.Application;
import pyatspi.registry.Registry;

class Atspi {
    static var reg:Registry = new Registry();
    static var accessibles:Array<Accessible> = [];
    public static var textField:Accessible;

    public static function getButtonsNames() {
        var a:Map<String, Accessible>= [];
        for ( aa in accessibles) {
            a[aa.get_name()] = aa;
        }
        return a;
    }

    public static function doAction(a:Accessible) {
        var act = a.queryAction();
        if (act != null) act.doAction(0);
    }

    public static function initialize() {
        reg.registerEventListener(function f(e) {accessibles= []; 
            try {
                printTree(0, e.source, accessibles);
            }   
            catch(e) {
                trace(e);
            }      
        }, ["window:activate"]);
		reg.registerEventListener(function f(e) {
            if (e.source.get_state_set().contains(Accessibility_Module.STATE_FOCUSED)) {
            textField = cast e.source; //trace("caret moved");Syntax.code("print(type({0}))", e.source); trace(e.source.queryText());trace(e.detail1);
            }
        }, ["object:text-caret-moved"]);
		reg.start(true);
        
    }

    public static function getActiveWindows() {
        var desktop:Accessible=  reg.getDesktop(0);
        
        trace(desktop.get_child_count());
        for (i in 0...desktop.get_child_count()) {
            var app = desktop.get_child_at_index(i);
            for (i in 0...app.get_child_count()) {
                var window = app.get_child_at_index(i);
                trace(window.get_state_set());
                if (window.get_state_set().contains(Accessibility_Module.STATE_ACTIVE)) {
                    Syntax.code("print(type({0}))", window);

                }
                printTree(0, window, []);
            }
        }
    }

    public static function printTree(index:Int = 0, root:Accessible, arr:Array<Accessible>) {
        if (root ==  null) return;
        if (!root.get_state_set().contains(Accessibility_Module.STATE_VISIBLE)) return;
        //trace(StringTools.lpad(""+root.get_role_name(), " ", (""+ root.get_role_name()).length + index));
        
        var text = root.get_text_iface();
        if (root.get_role_name()=="push button") {
            arr.push(root);
        }

        if ((root.get_role_name() == "page tab" ) && (!root.get_state_set().contains(Accessibility_Module.STATE_SELECTED)))  return;
        for (i in 0...root.get_child_count()) {
            var window = root.get_child_at_index(i);
            printTree(++index, window, arr );
        }
    }


}

@:pythonImport("Atspi", "Accessible")
extern class Accessible {
    public function get_name():String;
    public function get_action_iface():Action;
    public function get_child_at_index (child_index:Int):Accessible;
    public function get_child_count():Int;
    public function get_state_set():Dynamic;
    public function get_role_name():String;
    public function queryText():pyatspi.Text;
    public function queryAction():Action;
    public function get_text_iface():Text;

}
