import haxe.io.Path;

class Util {

    public static function absolutePathForFile(path:String, basePath:String = "") {
        var appPath =  (basePath== "") ?  Path.directory(Sys.programPath()) : Path.directory(basePath);

		var absPath = !Path.isAbsolute(path) ? appPath + "/" : "";
        absPath += path;

        return absPath;
    }
}