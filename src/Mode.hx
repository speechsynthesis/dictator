#if python
import python.Tuple.Tuple3;
import pyatspi.accessibility.Accessibility_Module;
#end
import sys.io.Process;

using StringTools;

interface Mode {
	public var modeName:ModeName;
	public function listensForCommand(sttResult:String, isPartial:Bool = true, alts:Array<Dynamic> = null):Mode;
}

enum abstract ModeName(String) {
	var dictation;
	var mode;
	var deaf;
	var spell;
	var general;
	#if python
	var navigation;
	var editor;
	#end


}

enum GeneralCommand {
	cancelCommand;     // Cancel what was said
	stop;              // Go back to choosing Modes
	hide;              // hide overlat
	help;              // show overlay


	key(keys:String, ?mode:ModeName);  // Presses keys
	simpleCommand(cmd:String, ?mode:ModeName);
}

enum ModeCommand {
	stopListening;
	writeHere;
	write;
	spell;
	#if python
	navigation;
	editor;
	#end
	quit; // exit : mode none
}

enum DeafCommand {
	startListening; // Starts listening
}

enum DictationCommand {
	// alternate;      // Select other alternative for recognized text mais oui
	// uppercase;      // write all in uppercase
	capitalize; // Capitalizes
	spell; // Change to spell mode
	// deleteLine;     // Delete Line
	deleteLastWord; // Ctrl back   ( ctrl w on vim)

	// deleteNextWord; // Ctrl del
	// goNextWord;     // ctrl arrow
	// selectNextWord; // ctrl shift arrow  paragraph
	// selectAll;      // ctrl a
	// alt d terminal ?
	//key(keys:String);
	word(word:String);

	// 
}

enum EditorCommand {
	goAfterSpecificWord;
	// go next word   // mot suivant suivant suivant ???
	// go previous word
	goWordStart;
	goWordEnd;
	goSentenceStart;
	goSentenceEnd;
	goLineStart;
	goLineEnd;
	selectWord;
	selectSentence;
	selectLine;
	// correct selection 
	// interactive correction with grammalecte
}

class BasicMode implements Mode {
	public static var commands:Map<String, ModeCommand> = [];
	public static var generalCommands:Map<String, GeneralCommand> = [];

	public var modeName = ModeName.mode;

	public function new() {
		trace("Passage en mode Basic");
		var commandDesc = [for (k in commands.keys()) k.rpad(" ", 20)].join("\n");
		commandDesc += "\n" + helperText(this);

		Helper.showText("Mode Basic", commandDesc);
	}

	public function listensForCommand(sttResult:String, isPartial:Bool = true, alts:Array<Dynamic> = null):Mode {
		if (BasicMode.checkGeneralCommand(sttResult, isPartial, alts)) return Dictator.mode;
		var mode = checkBasicCommand(this, sttResult, isPartial, alts);
		if (mode == null) return this;
		return mode;
	}

	public static function checkBasicCommand(mode:Mode, sttResult:String, isPartial:Bool = true, alts:Array<Dynamic> = null ): Mode {
		var hasFound = false;
		for (k => c in BasicMode.commands) {
			for (a in alts) {
				if (a.text == k) {
					hasFound = true;
					switch (c) {
						case quit:
							Helper.remove();
							Sys.exit(0);
						case stopListening:
							return new DeafMode();

						case write:
							STT.reset();
							Context.windowId = null;
							return new DictationMode();
						case writeHere:
							STT.reset();
							var p = new Process("xdotool getactivewindow");
							p.exitCode();
							Context.windowId = p.stdout.readAll().toString();
							var p = new Process("xdotool getactivewindow getwindowname");
							p.exitCode();
							Context.windowName = p.stdout.readAll().toString();
							var mode = new DictationMode();
							mode.selectWindowOption = true;
							return mode;
						#if python
						case navigation:
							return new NavigationMode();
						#end
						#if python
						case editor:
							return new EditorMode();
						#end
						case spell:
							return new SpellMode();
					}
					break;
				}
			}
			if (hasFound) break;
		}
		return null;
	}

	public static function checkGeneralCommand(sttResult:String, isPartial:Bool = true, alts:Array<Dynamic> = null ): Bool {
		var hasFound = false;
		for (k => c in BasicMode.generalCommands) {
			for (a in alts) {
				if (a.text == k) {
					hasFound = true;
					switch (c) {
						case cancelCommand:
							STT.reset();
						case stop:
							Dictator.mode = new BasicMode();
						case hide:
							Helper.remove();
						case help:
							Helper.activate();
						case key(k, m) if (m == null || m == ModeName.general || m == Dictator.mode.modeName):			
							STT.reset();
							trace(k + " " + m );
							Dictator.commandTool.key(k, 1);
						case simpleCommand(cmd, mode) if (mode == null || mode == Dictator.mode.modeName) :
							Sys.command(cmd);
						case _:
							trace("yaya" +c);
							hasFound = false;
					}
					break;
				}
			}
			if (hasFound) break;
		}
		return hasFound;
	}

	public static function helperText(mode:Mode) {
		var helpString = "";
		for (k=>v in BasicMode.generalCommands) {
			switch (v) {
				case key(keys,m) if (m != null && m != ModeName.general && m != mode.modeName):
					continue;
				case simpleCommand(cmd,m) if (m != null && m != ModeName.general && m != mode.modeName):
				default:
			}
			helpString += "^fg(red)"+ k +"\n";
		}
		return helpString;
	}
}

class DeafMode implements Mode {

	public var modeName = ModeName.deaf;
	public static var commands:Map<String, DeafCommand> = [];

	public function new() {
		trace("Passage en mode Sourd");
		var commandDesc = [for (k in commands.keys()) k.rpad(" ", 20)].join("\n");
		Helper.showText("Mode Deaf", commandDesc);
	}

	public function listensForCommand(sttResult:String, isPartial:Bool = true, alts:Array<Dynamic> = null):Mode {
		for (k => c in commands) {
			for (a in alts) {
				if (a.text == k) {
					switch (c) {
						case startListening:
							return new BasicMode();
						case _:
							return this;
					}
				}
			}
		}
		return this;
	}
}

class SpellMode implements Mode {

	public var modeName = ModeName.spell;
	public function new() {
		trace("Passage en mode Spell");
		Helper.showText("Mode Spell",BasicMode.helperText(this));
	}

	public function listensForCommand(sttResult:String, isPartial:Bool = true, alts:Array<Dynamic> = null):Mode {
		var hasFound = false;
		
		for (k => c in BasicMode.generalCommands) {
			for (a in alts) {
				if (a.text == k) {
					hasFound = true;
					switch (c) {
						case stop:
							trace("stop");
							return new DictationMode();
						case _:
							BasicMode.checkGeneralCommand(sttResult, isPartial, [a]);
					}
					break;
				}
			}
			if (hasFound) return Dictator.mode;
		}

		var words = sttResult.split(" ");

		var spelling = "";
		for (l in words) {
			if (l.length <= 0)
				continue;
			spelling += l.charAt(0);
		}
		Dictator.commandTool.type(spelling, 1);
		try {
			STT.reset();
		} catch (e) {
			trace(e);
		}

		return this;
	}
}

class DictationMode implements Mode {

	public var modeName = ModeName.dictation;
	public static var commands:Map<String, DictationCommand> = [];

	var doCapitalize = false;
	var doCorrectGrammar = false;
	var doCorrectSpelling = false;
	var doPunctuation = true;
	var doCapitalizeNextWord = false;

	var lastTypeProcess:Process = null;

	var appId:String = "";

	public var selectWindowOption = false; // Only one specific windows is chosen and it is managed by xdoTool

	public function new() {
		trace("Passage en mode Dictée");
		var commandDesc = [for (k in commands.keys()) k].join("\n");
		commandDesc += "\n" + BasicMode.helperText(this);
		var windowName:String = (Context.windowId == null) ? "" : Context.windowName.substr(0, 8);
		Helper.showText("Mode Dictée " + windowName, commandDesc);
	}

	public function listensForCommand(sttResult:String, isPartial:Bool = true, alts:Array<Dynamic> = null):Mode {
		// trace(sttResult + "__" + lastPartialText + "__");

		if (BasicMode.checkGeneralCommand(sttResult, isPartial, alts)) {
			lastPartialText = "";
			return Dictator.mode;
		}
		//var mode = BasicMode.checkBasicCommand(this, sttResult, isPartial, alts);
		//if (mode != null) return mode;
		var hasFound = false;
		for (k => c in DictationMode.commands) {
			for (a in alts) {
				if (a.text == k) {
					hasFound = true;
					switch (c) {
						case capitalize:
							STT.reset();
							doCapitalizeNextWord = true;
							return this;
						/*
						case key(k):
							lastPartialText = "";
							Dictator.commandTool.key(k, 1);
							STT.reset();
							return this;*/
						case word(w):
							type(w, isPartial);
							lastPartialText = w;
							if (!isPartial)
								lastPartialText = "";
							return this;
						case deleteLastWord:
							STT.reset();
							Dictator.commandTool.key("ctrl+BackSpace", 1);
							return this;
						case spell:
							lastPartialText = "";
							STT.reset();
							return new SpellMode();
						case _:
							lastPartialText = sttResult;
							return this;
					}
					break;
				}
			}
			if (hasFound) {
				break;
			}
		}

		if (isPartial && (lastTypeProcess != null)) {
			if (lastTypeProcess.exitCode(true) == null)
				return this;
		}

		if (!selectWindowOption) {
			if (lastPartialText == "") {
				appId = getActiveWindowId();
			} else {
				if (appId != getActiveWindowId()) {
					// stops here
					// it shouldn't type in a window. where it hasn't started
					// too easy to make mistakes
					lastPartialText = "";
					STT.reset();
					return this;
				}
			}
		}

		sttResult = " " + sttResult;

		if (!isPartial && Locale.instance.grammar != null) {
			trace(Locale.instance.grammar);
			Locale.instance.grammar.correctGrammar(sttResult, this);
			lastPartialText = "";
			return this;
		}

		if (sttResult == lastPartialText) {
			if (!isPartial) {
				lastPartialText = "";
				// doCapitalizeNextWord = false;
			}

			return this;
		}

		type(sttResult, isPartial);

		lastPartialText = sttResult;

		if (!isPartial) {
			lastPartialText = "";
		}

		return this;
	}

	private var lastPartialText:String = "";

	public function type(text:String, isPartial:Bool) {
		var iStart = -1;

		for (i in 0...text.length) {
			if (text.charAt(i) != lastPartialText.charAt(i)) {
				iStart = i;
				break;
			}
		}

		if (iStart == -1) {
			if (lastPartialText == "")
				iStart = 0;
			else {
				// if (!isPartial) doCapitalizeNextWord = false;
				return null;
			}
		}

		for (x in 0...(lastPartialText.length - iStart)) {
			Dictator.commandTool.key("BackSpace", 1);
		}
		trace("before:_" + lastPartialText + "_ aim:_" + text + "_ needsonly_" + text.substr(iStart) + "_");

		var stringToType = text.substr(iStart);
		if (doCapitalizeNextWord) {
			function capitalize(s:String) {
				for (i in 0...stringToType.length) {
					var l = stringToType.charAt(i);
					if (![" ", ",", "."].contains(l)) {
						return stringToType.substr(0, i) + l.toUpperCase() + stringToType.substr(i + 1);
					}
				}
				return stringToType;
			}
			stringToType = capitalize(stringToType);
			if (!isPartial)
				doCapitalizeNextWord = false;
		}
		return Dictator.commandTool.type(stringToType, 1);
	}

	public static inline function replacePart(original:String, start:Int, end:Int, s:String) {
		return original.substring(0, start) + s + original.substring(end);
	}

	public function getActiveWindowId() {
		var process = new sys.io.Process("xdotool getactivewindow");
		process.exitCode();
		var id = process.stdout.readAll().toString();
		return id;
	}
}

#if python
class NavigationMode implements Mode {
	public var modeName = ModeName.deaf;
	public var buttonNames:Map<String, Atspi.Accessible> = [];

	public function new() {
		trace("Passage en mode Navigation");
		var commandDesc = [for (k in (buttonNames = Atspi.getButtonsNames()).keys()) k.rpad(" ", 20)].join("\n");
		commandDesc += "\n" + BasicMode.helperText(this);
		Helper.showText("Mode navigation", commandDesc);
	}

	public function listensForCommand(sttResult:String, isPartial:Bool = true, alts:Array<Dynamic> = null):Mode {
		if (BasicMode.checkGeneralCommand(sttResult, isPartial, alts)) return Dictator.mode;
		//var mode = BasicMode.checkBasicCommand(this, sttResult, isPartial, alts);
		//if (mode!= null) return mode;
		for (k => c in buttonNames) {
			for (a in alts) {
				if (a.text == k) {
					Atspi.doAction(c);
					break;
				}
			}
		}
		return this;
	}
}

class EditorMode implements Mode {
	public var modeName = ModeName.editor;
	public static var commands:Map<String, EditorCommand> = [];

	public function new() {
		trace("Passage en mode Edition");
		// var commandDesc = [for (k in (buttonNames=Atspi.getButtonsNames()).keys()) k.rpad(" ", 20)].join("\n");
		var commandDesc = [for (k in commands.keys()) k].join("\n");
		commandDesc += "\n" + BasicMode.helperText(this);
		Helper.showText("Mode édition", commandDesc);
	}

	public function listensForCommand(sttResult:String, isPartial:Bool = true, alts:Array<Dynamic> = null):Mode {
		if (BasicMode.checkGeneralCommand(sttResult, isPartial, alts)) return Dictator.mode;
		//var mode = BasicMode.checkBasicCommand(this, sttResult, isPartial, alts);
		//if (mode!= null) return mode;
		if (Atspi.textField== null) return this;
		var textO = Atspi.textField.queryText();
		if (textO == null) return this;
		var found = false;
		for (k => c in EditorMode.commands) {
			for (a in alts) {
				if (StringTools.startsWith(a.text, k)) {
					switch (c) {
						case goAfterSpecificWord:
							var text:String = a.text;
							text = StringTools.trim(text.substr(k.length));
							var t = textO.getText(0, -1);
							textO.setCaretOffset(t.indexOf(text) + text.length);
							// TODO devrait selectionner le plus proche du caret
							textO.setCaretOffset(t.indexOf(text) + text.length);
						case goWordStart:
							var t:Tuple3<String, Int, Int> = textO.getTextAtOffset(textO.get_caretOffset() - 1, Accessibility_Module.TEXT_BOUNDARY_WORD_START);
							textO.setCaretOffset(t._2);
						case goWordEnd:
							var t:Tuple3<String, Int, Int> = textO.getTextAtOffset(textO.get_caretOffset(), Accessibility_Module.TEXT_BOUNDARY_WORD_END);
							textO.setCaretOffset(t._3);
						case goSentenceStart:
							try {
							var t:Tuple3<String, Int, Int> = textO.getTextAtOffset(textO.get_caretOffset(), Accessibility_Module.TEXT_BOUNDARY_SENTENCE_START);
							textO.setCaretOffset(t._2);}
							catch(e) {
								trace(e);
							}
						case goSentenceEnd:
							var t:Tuple3<String, Int, Int> = textO.getTextAtOffset(textO.get_caretOffset(), Accessibility_Module.TEXT_BOUNDARY_SENTENCE_END);
							textO.setCaretOffset(t._3);
						case goLineStart:
							var t:Tuple3<String, Int, Int> = textO.getTextAtOffset(textO.get_caretOffset(), Accessibility_Module.TEXT_BOUNDARY_LINE_START);
							textO.setCaretOffset(t._2);
						case goLineEnd:
							var t:Tuple3<String, Int, Int> = textO.getTextAtOffset(textO.get_caretOffset(), Accessibility_Module.TEXT_BOUNDARY_LINE_END);
							textO.setCaretOffset(t._3);
						case selectWord:
							var t:Tuple3<String, Int, Int> = textO.getTextAtOffset(textO.get_caretOffset(), Accessibility_Module.TEXT_BOUNDARY_WORD_START);
							var selectword = textO.setSelection(0, t._2, t._3);
							trace(selectword);
						case selectSentence:
							var t:Tuple3<String, Int, Int> = textO.getTextAtOffset(textO.get_caretOffset(), Accessibility_Module.TEXT_BOUNDARY_SENTENCE_START);
							textO.setSelection(0, t._2, t._3);
						case selectLine:
							var t:Tuple3<String, Int, Int> = textO.getTextAtOffset(textO.get_caretOffset(), Accessibility_Module.TEXT_BOUNDARY_LINE_START);
							textO.setSelection(0, t._2, t._3);
					}
					trace("found for" + k );
					found = true;
					break;
				}
			}
			if (found) break;
		}

		return this;
	}
}
#end
