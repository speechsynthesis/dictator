import Mode;
import sys.io.File;
import sys.FileSystem;
import haxe.EnumTools;

class Locale {
	public static var instance:Locale;

	public var configPath:String;

	public var modelPath:String;
	public var grammar:Grammar = null;

	public function new() {
		instance = this;
	}

	public function load(lang:String) {
		var configPath = Util.absolutePathForFile("config.ini");
		if (!FileSystem.exists(configPath))
			configPath = Sys.getEnv("HOME") + ("/.config/dictator/config.ini");
		if (!FileSystem.exists(configPath)) {
			Sys.println("no config file");
			Sys.println("you can create a config.ini in" + configPath);

			Sys.exit(1);
		}
		var ini = hxIni.IniManager.loadFromFile(Util.absolutePathForFile("config.ini"))[lang];
		if (ini == null) {
			Sys.println('the configuration $lang doesn\'t exist in the config file ' + configPath);
			Sys.exit(1);
		}

		var commandFile = ini["commandsfile"];
		modelPath = ini["model"];
		grammar = switch (ini["grammar"]) {
			case null:
				null;
			case "grammalecte":
				new Grammar.Grammalecte();
			case _:
				null;
		}

		var commandPath = Util.absolutePathForFile(commandFile, configPath);
		if (!FileSystem.exists(commandPath)) {
			Sys.println('$commandPath doesn\'t exist');
		}
		var csvF = File.getContent(Util.absolutePathForFile(commandFile, configPath));
		var commands = format.csv.Reader.parseCsv(csvF, ";");

		function checkGeneralCommand(textCommand:String, enumS:String, enumParam:Null<Array<Dynamic>>, modeString:String) {
			if (!EnumTools.getConstructors(GeneralCommand).contains(enumS)) {
				return false;
			}
			
			if (enumParam!= null && (modeString != "" )&&  (modeString != null )) {
				enumParam.push(modeString);
			}

			try {
			BasicMode.generalCommands[textCommand] = EnumTools.createByName(GeneralCommand, enumS, enumParam);
			}
			catch (e) {
				BasicMode.generalCommands[textCommand] = EnumTools.createByName(GeneralCommand, enumS);
			}
			return true;

		}

		for (l in commands) {
			var mode:String  = cast  l[0];
			var enumS:String = "" + l[1];

			var iindex = enumS.indexOf(":");
			var enumParam:Array<String> = (iindex != -1) ? [enumS.substring(iindex + 1)] : [];
			if (iindex != -1)
				enumS = enumS.substring(0, iindex);

			var textCommand = l[2];
			switch (mode) {
				case "general":
					if (!EnumTools.getConstructors(GeneralCommand).contains(enumS))
						continue;
					BasicMode.generalCommands[textCommand] = EnumTools.createByName(GeneralCommand, enumS, enumParam);
				case "mode":
					if (checkGeneralCommand(textCommand,enumS, enumParam, mode)) continue;
					if (!EnumTools.getConstructors(ModeCommand).contains(enumS))
						continue;
					BasicMode.commands[textCommand] = EnumTools.createByName(ModeCommand, enumS, enumParam);
				case "dictation":
					if (checkGeneralCommand(textCommand,enumS, enumParam, mode)) continue;
					if (!EnumTools.getConstructors(DictationCommand).contains(enumS))
						continue;
					DictationMode.commands[textCommand] = EnumTools.createByName(DictationCommand, enumS, enumParam);
				case "deaf":
					if (checkGeneralCommand(textCommand,enumS, enumParam, mode)) continue;
					if (!EnumTools.getConstructors(DeafCommand).contains(enumS))
						continue;
					DeafMode.commands[textCommand] = EnumTools.createByName(DeafCommand, enumS, enumParam);
				#if python
				case "editor":
					if (checkGeneralCommand(textCommand,enumS, enumParam, mode)) continue;
					if (!EnumTools.getConstructors(EditorCommand).contains(enumS))
						continue;
					EditorMode.commands[textCommand] = EnumTools.createByName(EditorCommand, enumS, enumParam); #end
			}
		}
	}
}
