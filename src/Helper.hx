import sys.io.Process;
import sys.io.File;

class Helper {
	static var process:Process;
	static var showHelp:Bool = true;
	static var lastCommand:String;

	static var templateS = " (
		echo '::title::' # Fist line goes to title
		# The following lines go to slave window
		::foreach lines::
		echo \"::line::\"
		::end::
		) | dzen2 -x \"500\" -y \"30\" -w \"220\" -l \"::number_lines::\" -sa 'l' -ta 'c' \\
		   -p -e 'onstart=uncollapse;'";

	public static function killLastProcess() {
		var originalPid = process.getPid();
		var pro = new sys.io.Process("pgrep -P " + originalPid);
		pro.exitCode();
		var pid = StringTools.trim(pro.stdout.readAll().toString());
		process.kill();
		Sys.command("kill " + pid);
		process.close();
	}

	public static function showText(title:String = "", s:String = "") {
		if (process != null) {
			killLastProcess();
		}

		var lines:Array<{line:String}> = [];
		for (l in s.split("\n")) {
			lines.push({line: l});
		}

		var template = new haxe.Template(templateS);
		var command = template.execute({lines: lines, number_lines: s.split("\n").length, title: title});

		lastCommand = command;

		if (!showHelp)
			return;

		process = new sys.io.Process(command);
	}

	public static function remove() {
		showHelp = false;
		if (process != null) {
			killLastProcess();
		}

		process = null;
	}

	public static function activate() {
		showHelp = true;
		if (process == null)
			process = new sys.io.Process(lastCommand);
	}
}
