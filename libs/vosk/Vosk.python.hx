package vosk;

class Vosk {
    public static function voskRecognizerReset(v:VoskRecognizer):Dynamic {
        return v.reset();
    }

    public static function voskRecognizerResult(recognizer:VoskRecognizer):String {
        return recognizer.result();
    }

    public static function voskRecognizerPartialResult(recognizer:VoskRecognizer):String {
        return recognizer.partialResult();
    }

    public static function voskModelNew(modelPath:String):VoskModel {
        return new VoskModel(modelPath);
    }

    public static function voskModelFindWord(model:VoskModel,  word:String):Int {
        return model.FindWord(word);
    }

    public static function voskRecognizerNew(model:VoskModel, sampleRate:Float):VoskRecognizer {
        return new VoskRecognizer(model, sampleRate);
    }

    public static function voskRecognizerNewGrm(model:VoskModel, sampleRate:Float, grammar:String){
        return VoskRecognizer.newGrm(model, sampleRate, grammar);
    }

    public static function voskRecognizerSetMaxAlternatives(recognizer:VoskRecognizer, maxAlternatives:Int){
        recognizer.setMaxAlternatives(maxAlternatives);
    }

    public static function voskRecognizerSetWords(recognizer:VoskRecognizer, words:Int):Bool {
        return recognizer.setWords(words);
    }

    public static function voskRecognizerSetPartialWords(recognizer:VoskRecognizer, words:Int):Bool {
        return recognizer.setPartialWords(words);
    }

    public static function voskRecognizerAcceptWaveform(recognizer:VoskRecognizer, data, length:Int):Int {
        
        var view = python.Syntax.code("bytes(data)");
        return recognizer.acceptWaveform(view);
    }




}

@:pythonImport("vosk.Vosk")
extern class VoskV {
    /*
    static function client(service_name :String, ?options :KwArgs<Boto3ClientOptions>) :Dynamic;

    // @:native("get_paginator")
    // static function getPaginator(name :String) :EitherType<ListTablesPaginator,EitherType<QueryPaginator,ScanPaginator>>;

    @:native("get_waiter")
    static function getWaiter(name :String) :Waiter;

    @:native("session.Session")
    static function session(?options :KwArgs<Boto3SessionOptions>) :Session;*/

    
}


@:pythonImport("vosk","KaldiRecognizer")
extern class VoskRecognizer {

    @:native("Reset")
    function reset():Dynamic;

    @:native("__new__")
    function new(model:VoskModel, sampleRate:Float);

    @:native("NewGrm")
    static function newGrm(model:VoskModel, sampleRate:Float, grammar:String):VoskRecognizer;

    @:native("SetMaxAlternatives")
    function setMaxAlternatives(maxAlternatives:Int):Void;

    @:native("SetWords")
    function setWords(words:Int):Bool;

    @:native("SetPartialWords")
    function setPartialWords(words:Int):Bool;

    @:native("AcceptWaveform")
    function acceptWaveform(data:Dynamic):Int;

    @:native("Result")
    function result():String;

    @:native("PartialResult")
    function partialResult():String;
    




    /*
    static function client(service_name :String, ?options :KwArgs<Boto3ClientOptions>) :Dynamic;

    // @:native("get_paginator")
    // static function getPaginator(name :String) :EitherType<ListTablesPaginator,EitherType<QueryPaginator,ScanPaginator>>;

    @:native("get_waiter")
    static function getWaiter(name :String) :Waiter;

    @:native("session.Session")
    static function session(?options :KwArgs<Boto3SessionOptions>) :Session;*/
}

@:pythonImport("vosk", "Model")
extern class VoskModel {

    @:native("__new__")
    function new(modelPath:String);

    @:native("FindWord")
    public function FindWord(word:String):Int;
    /*
    static function client(service_name :String, ?options :KwArgs<Boto3ClientOptions>) :Dynamic;

    // @:native("get_paginator")
    // static function getPaginator(name :String) :EitherType<ListTablesPaginator,EitherType<QueryPaginator,ScanPaginator>>;

    @:native("get_waiter")
    static function getWaiter(name :String) :Waiter;

    @:native("session.Session")
    static function session(?options :KwArgs<Boto3SessionOptions>) :Session;*/
}