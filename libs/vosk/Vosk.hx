package vosk;

#if !macro
import cpp.*;

@:build(vosk.Vosk.VoskMacro.addBuildConfig())
@:include('vosk_api.h')
extern class Vosk {

	/**
	 * Load model data from the file and returns the model object
	 * @param model_path: the path of the model on the filesystem
	 * @return model object or NULL if problem occured 
	 */
	@:native('vosk_model_new')
	static function voskModelNew(modelPath:String):VoskModel;
	



	/** Releases the model memory
 	 *
 	 *  The model object is reference-counted so if some recognizer
 	 *  depends on this model, model might still stay alive. When
 	 *  last recognizer is released, model will be released too.
	 */
	@:native('vosk_model_free')
	static function voskModelFree(model:VoskModel):Void;

	/**
	 * Check if a word can be recognized by the model
	 * @param word : the word
	 * @return the word symbol if @param word exists inside the model
	 * or -1 otherwise.
	 * Reminding that word symbol 0 is for <epsilon>
	 */
	@:native('vosk_model_find_word')
	static function voskModelFindWord(model:VoskModel,  word:String):Int;

	/**
	 * Loads speaker model data from the file and returns the model object
	 * @param model_path : the path of the model on the filesystem
	 * @return model object or NULL if problem occured
	 */
	@:native('vosk_spk_model_new')
	static function voskSpkModelNew(modelPath:String):VoskSpkModel;

	/**
	 * Releases the model memory
	 *
	 *  The model object is reference-counted so if some recognizer
	 *  depends on this model, model might still stay alive. When
	 *  last recognizer is released, model will be released too. 
	 **/
	@:native('vosk_spk_model_free')
	static function voskSpkModelFree(model:VoskSpkModel):Void;


	/**
	 * Creates the recognizer object
	 * The recognizers process the speech and return text using shared model data
	 * @param model VoskModel containing static data for recognizer. Model can be
	 * shared across recognizers, even running in different threads.
	 * @param sampleRate The sample rate of the audio you going to feed into the recognizer.
	 * Make sure this rate matches the audio content, it is a common issue causing accuracy problems.
	 * @return Star<VoskRecognizer>
	 */
	@:native('vosk_recognizer_new')
	static function voskRecognizerNew(model:VoskModel, sampleRate:Float):VoskRecognizer;

	/**
	 * Creates the recognizer object with speaker recognition
	 * With the speaker recognition mode the recognizer not just recognize
	 * text but also return speaker vectors one can use for speaker identification
	 * @param model VoskModel containing static data for recognizer. Model can be
	 * shared across recognizers, even running in different threads.
	 * @param sampleRate The sample rate of the audio you going to feed into the recognizer.
	 * Make sure this rate matches the audio content, it is a common issue causing accuracy problems.
	 * @param spkModel speaker model for speaker identification
	 * @return recognizer object or NULL if problem occured
	 */
	@:native('vosk_recognizer_new_spk')
	static function voskRecognizerNewSpk(model:VoskModel, sampleRate:Float,spkModel:VoskSpkModel):VoskRecognizer;

	/**
	 * Creates the recognizer object with the phrase list
	 * Sometimes when you want to improve recognition accuracy and when you don't need
	 * to recognize large vocabulary you can specify a list of phrases to recognize. This
	 * will improve recognizer speed and accuracy but might return [unk] if user said
	 * something different.
	 * 
	 * Only recognizers with lookahead models support this type of quick configuration.
	 * Precompiled HCLG graph models are not supported.
	 * @param model VoskModel containing static data for recognizer. Model can be
	 * shared across recognizers, even running in different threads.
	 * @param sampleRate The sample rate of the audio you going to feed into the recognizer.
	 * Make sure this rate matches the audio content, it is a common issue causing accuracy problems
	 * @param grammar The string with the list of phrases to recognize as JSON array of strings,
	 * for example "["one two three four five", "[unk]"]".
	 * @return Star<VoskRecognizer> recognizer object or NULL if problem occured
	 */
	@:native('vosk_recognizer_new_grm')
	static function voskRecognizerNewGrm(model:VoskModel, sampleRate:Float, grammar:String):VoskRecognizer;

	@:native('vosk_recognizer_set_spk_model')
	static function voskRecognizerSetSpkModel(recognizer:VoskRecognizer, spk_model:VoskSpkModel):Void;

	/** Reconfigures recognizer to use grammar
	*
	* @param recognizer   Already running VoskRecognizer
	* @param grammar      Set of phrases in JSON array of strings or "[]" to use default model graph.
	*                     See also vosk_recognizer_new_grm
	*/
	@:native('vosk_recognizer_set_grm')
	static function voskRecognizerSetGrm(model:VoskModel, grammar:String):VoskRecognizer;

	/**
	 * Configures recognizer to output n-best results
	 * <pre>
	 *   {
	 *      "alternatives": [
	 *          { "text": "one two three four five", "confidence": 0.97 },
	 *          { "text": "one two three for five", "confidence": 0.03 },
	 *      ]
	 *   }
	 * </pre>
	 * 
	 * @param recognizer 
	 * @param maxAlternatives 
	 */
	@:native('vosk_recognizer_set_max_alternatives')
	static function voskRecognizerSetMaxAlternatives(recognizer:VoskRecognizer, maxAlternatives:Int):Void;

	/**
	 * Enables words with times in the output
	 * <pre>
	 *   "result" : [{
	 *       "conf" : 1.000000,
	 *       "end" : 1.110000,
	 *       "start" : 0.870000,
	 *       "word" : "what"
	 *     }, {
	 *       "conf" : 1.000000,
	 *       "end" : 1.530000,
	 *       "start" : 1.110000,
	 *       "word" : "zero"
	 *     }, {
	 *       "conf" : 1.000000,
	 *       "end" : 1.950000,
	 *       "start" : 1.530000,
	 *       "word" : "zero"
	 *     }, {
	 *       "conf" : 1.000000,
	 *       "end" : 2.340000,
	 *       "start" : 1.950000,
	 *       "word" : "zero"
	 *     }, {
	 *       "conf" : 1.000000,
	 *       "end" : 2.610000,
	 *       "start" : 2.340000,
	 *       "word" : "one"
	 *     }],
	 * </pre>
	 * @param recognizer 
	 * @param words 
	 * @return Boolean
	 */
	@:native('vosk_recognizer_set_words')
	static function voskRecognizerSetWords(recognizer:VoskRecognizer, words:Int):Bool;


	/** Like above return words and confidences in partial results
	*
	* @param partial_words - boolean value
	*/
	@:native('vosk_recognizer_set_partial_words')
	static function voskRecognizerSetPartialWords(recognizer:VoskRecognizer, partialWords:Int):Bool;

	/**
	 * Accept voice data
	 * accept and process new chunk of voice data
	 * @param recognizer 
	 * @param data  audio data in PCM 16-bit mono format
	 * @param length length of the audio data
	 * @return Int 1 if silence is occured and you can retrieve a new utterance with result method 
	 *  0 if decoding continues
	 * -1 if exception occured
	 */
	@:native('vosk_recognizer_accept_waveform')
	static function voskRecognizerAcceptWaveform(recognizer:VoskRecognizer, data:ConstCharStar, length:Int):Int;

	/**
	 * Same as above but the version with the short data for language bindings where you have
	 * audio as array of shorts
	 * @param recognizer 
	 * @param data 
	 * @param length 
	 * @return Int
	 */
	@:native('vosk_recognizer_accept_waveform_s')
	static function voskRecognizerAcceptWaveformS(recognizer:VoskRecognizer, data:ConstStar<UInt8>, length:Int):Int;

		/**
	 * Same as above but the version with the float data for language bindings where you have
	 * audio as array of floats
	 * @param recognizer 
	 * @param data 
	 * @param length 
	 * @return Int
	 */
	@:native('vosk_recognizer_accept_waveform_f')
	static function voskRecognizerAcceptWaveformF(recognizer:VoskRecognizer, data:String, length:Int):Int;

	/**
	 * Returns speech recognition result
	 * @param recognizer 
	 * @return String the result in JSON format which contains decoded line, decoded
	 * words, times in seconds and confidences. You can parse this result
	 * with any json parser
	 * <pre>
 	 *  {
 	 *    "text" : "what zero zero zero one"
 	 *  }
 	 * </pre>
 	 *
 	 * If alternatives enabled it returns result with alternatives, see also vosk_recognizer_set_alternatives().
 	 *
 	 * If word times enabled returns word time, see also vosk_recognizer_set_word_times().
	 */
	@:native('vosk_recognizer_result')
	static function voskRecognizerResult(recognizer:VoskRecognizer):String;

	/**
	 * Returns partial speech recognition
	 * @param recognizer 
	 * @return String partial speech recognition text which is not yet finalized.
	 *          result may change as recognizer process more data.
	 * <pre>
	 * {
	 *    "partial" : "cyril one eight zero"
	 * }
	 * </pre>
	 */
	@:native('vosk_recognizer_partial_result')
	static function voskRecognizerPartialResult(recognizer:VoskRecognizer):String;

	/**
	 * Returns speech recognition result. Same as result, but doesn't wait for silence
	 * You usually call it in the end of the stream to get final bits of audio. It
	 * flushes the feature pipeline, so all remaining audio chunks got processed.
	 * @param recognizer 
	 * @return String   speech result in JSON format.
	 */
	@:native('vosk_recognizer_final_result')
	static function voskRecognizerFinalResult(recognizer:VoskRecognizer):String;

	/**
	 * Resets the recognizer
	 * Resets current results so the recognition can continue from scratch
	 * @param recognizer 
	 */
	@:native('vosk_recognizer_reset')
	static function voskRecognizerReset(recognizer:VoskRecognizer):Void;

	/**
	 * Releases recognizer object
	 * Underlying model is also unreferenced and if needed released
	 * @param recognizer 
	 */
	@:native('vosk_recognizer_free')
	static function voskRecognizerFree(recognizer:VoskRecognizer):Void;

	/**
	 * Set log level for Kaldi messages
	 * @param logLevel the level
	 * 0 - default value to print info and error messages but no debug
	 * less than 0 - don't print info messages
	 * greather than 0 - more verbose mode
	 */
	@:native('vosk_set_log_level')
	static function voskSetLogLevel(logLevel:Int):Void;

	/**
	 * Init, automatically select a CUDA device and allow multithreading.
	 * Must be called once from the main thread.
	 * Has no effect if HAVE_CUDA flag is not set.
	 */
	@:native('vosk_gpu_init')
	static function voskGpuInit():Void;

	/**
	 * Init CUDA device in a multi-threaded environment.
	 * Must be called for each thread.
	 * Has no effect if HAVE_CUDA flag is not set.
	 */
	@:native('vosk_gpu_thread_init')
	static function voskGpuThreadInit():Void;
}

typedef VoskModel = Star<InternalVoskModel>;


@:structAccess
@:unreflective
@:include('vosk_api.h')
@:extern('VoskModel')
@:native('VoskModel')
extern class InternalVoskModel {

}

typedef VoskSpkModel = Star<InternalVoskSpkModel>;

@:structAccess
@:unreflective
@:include('vosk_api.h')
@:extern('VoskSpkModel')
@:native('VoskSpkModel')
extern class InternalVoskSpkModel {

}

typedef VoskRecognizer = Star<InternalVoskRecognizer>;

@:structAccess
@:unreflective
@:extern('VoskRecognizer')
@:native('VoskRecognizer')
@:include('vosk_api.h')
extern class InternalVoskRecognizer {

}

#else
// #macro

import haxe.macro.Context;
import haxe.io.Path;

class VoskMacro {

	static function addBuildConfig() {
		var classPosInfo = Context.getPosInfos(Context.currentPos());
		var classFilePath = sys.FileSystem.absolutePath(Context.resolvePath(classPosInfo.file));
        var classDir = Path.directory(classFilePath);
		var buildXml = '
		<files id="haxe">	
		<compilerflag value="-I$classDir/include"/>
		</files>
		<target id="haxe">
			<libpath name="$classDir/libs"/>
			<lib name="-lvosk" />
		</target>
		';
		
		// add @:buildXml
		Context.getLocalClass().get().meta.add(':buildXml', [macro $v{buildXml}], Context.currentPos());

		return Context.getBuildFields();
	}

}

#end
