/* This file is generated, do not edit! */
package pyatspi.action;
@:pythonImport("pyatspi.action") extern class Action_Module {
	static public var Atspi : Dynamic;
	static public var __all__ : Dynamic;
	static public var __builtins__ : Dynamic;
	static public var __cached__ : Dynamic;
	static public var __doc__ : Dynamic;
	static public var __file__ : Dynamic;
	static public var __loader__ : Dynamic;
	static public var __name__ : Dynamic;
	static public var __package__ : Dynamic;
	static public var __spec__ : Dynamic;
	/**
		Generates all possible keyboard modifiers for use with 
		L{registry.Registry.registerKeystrokeListener}.
	**/
	static public function allModifiers():Dynamic;
	static public function attributeListToHash(list:Dynamic):Dynamic;
	static public function clearCache():Dynamic;
	/**
		Searches for all descendant nodes satisfying the given predicate starting at 
		this node. Does an in-order traversal. For example,
		
		pred = lambda x: x.getRole() == pyatspi.ROLE_PUSH_BUTTON
		buttons = pyatspi.findAllDescendants(node, pred)
		
		will locate all push button descendants of node.
		
		@param acc: Root accessible of the search
		@type acc: Accessibility.Accessible
		@param pred: Search predicate returning True if accessible matches the 
		                search criteria or False otherwise
		@type pred: callable
		@return: All nodes matching the search criteria
		@rtype: list
	**/
	static public function findAllDescendants(acc:Dynamic, pred:Dynamic):Dynamic;
	/**
		Searches for an ancestor satisfying the given predicate. Note that the
		AT-SPI hierarchy is not always doubly linked. Node A may consider node B its
		child, but B is not guaranteed to have node A as its parent (i.e. its parent
		may be set to None). This means some searches may never make it all the way
		up the hierarchy to the desktop level.
		
		@param acc: Starting accessible object
		@type acc: Accessibility.Accessible
		@param pred: Search predicate returning True if accessible matches the 
		        search criteria or False otherwise
		@type pred: callable
		@return: Node matching the criteria or None if not found
		@rtype: Accessibility.Accessible
	**/
	static public function findAncestor(acc:Dynamic, pred:Dynamic):Dynamic;
	/**
		Searches for a descendant node satisfying the given predicate starting at 
		this node. The search is performed in depth-first order by default or
		in breadth first order if breadth_first is True. For example,
		
		my_win = findDescendant(lambda x: x.name == 'My Window')
		
		will search all descendants of x until one is located with the name 'My
		Window' or all nodes are exausted. Calls L{_findDescendantDepth} or
		L{_findDescendantBreadth} to start the recursive search.
		
		@param acc: Root accessible of the search
		@type acc: Accessibility.Accessible
		@param pred: Search predicate returning True if accessible matches the 
		                search criteria or False otherwise
		@type pred: callable
		@param breadth_first: Search breadth first (True) or depth first (False)?
		@type breadth_first: boolean
		@return: Accessible matching the criteria or None if not found
		@rtype: Accessibility.Accessible or None
	**/
	static public function findDescendant(acc:Dynamic, pred:Dynamic, ?breadth_first:Dynamic):Dynamic;
	static public function getBoundingBox(rect:Dynamic):Dynamic;
	static public function getCacheLevel():Dynamic;
	/**
		Gets the ID of an interface class or object in string format for use in
		queryInterface.
		
		@param obj: Class representing an AT-SPI interface or instance
		@type obj: object
		@return: IID for the interface
		@rtype: string
		@raise AttributeError: When the parameter does not provide typecode info
		
		WARNING!! DEPRECATED!!
		
		In current D-Bus version of pyatspi this simply returns a null string.
	**/
	static public function getInterfaceIID(obj:Dynamic):Dynamic;
	/**
		Gets the human readable name of an interface class or object in string
		format.
		
		@param obj: Class representing an AT-SPI interface or instance
		@type obj: class
		@return: Name of the interface
		@rtype: string
		@raise AttributeError: When the parameter does not provide typecode info
	**/
	static public function getInterfaceName(obj:Dynamic):Dynamic;
	/**
		Gets the path from the application ancestor to the given accessible in
		terms of its child index at each level.
		
		@param acc: Target accessible
		@type acc: Accessibility.Accessible
		@return: Path to the target
		@rtype: list of integer
		@raise LookupError: When the application accessible cannot be reached
	**/
	static public function getPath(acc:Dynamic):Dynamic;
	static public function hashToAttributeList(h:Dynamic):Dynamic;
	/**
		Gets a list of the names of all interfaces supported by this object. The
		names are the short-hand interface names like "Accessible" and "Component",
		not the full interface identifiers.
		
		@param obj: Arbitrary object to query for all accessibility related
		interfaces. Must provide a queryInterface method.
		@type obj: object
		@return: Set of supported interface names
		@rtype: set
		@raise AttributeError: If the object provide does not implement
		queryInterface
	**/
	static public function listInterfaces(obj:Dynamic):Dynamic;
	static public function pointToList(point:Dynamic):Dynamic;
	static public function printCache():Dynamic;
	static public function rectToList(rect:Dynamic):Dynamic;
	/**
		Converts a relation value to a string based on the name of the state constant
		in the L{constants} module that has the given value.
		
		@param value: An AT-SPI relation
		@type value: Accessibility.RelationType
		@return: Human readable, untranslated name of the relation
		@rtype: string
	**/
	static public function relationToString(value:Dynamic):Dynamic;
	static public function setCacheLevel(level:Dynamic):Dynamic;
	/**
		Converts a state value to a string based on the name of the state constant in
		the L{constants} module that has the given value.
		
		@param value: An AT-SPI state
		@type value: Accessibility.StateType
		@return: Human readable, untranslated name of the state
		@rtype: string
	**/
	static public function stateToString(value:Dynamic):Dynamic;
	/**
		Maps a string name to an AT-SPI constant. The rules for the mapping are as 
		follows:
		        - The prefix is captalized and has an _ appended to it.
		        - All spaces in the suffix are mapped to the _ character. 
		        - All alpha characters in the suffix are mapped to their uppercase.
		
		The resulting name is used with getattr to look up a constant with that name
		in the L{constants} module. If such a constant does not exist, the string
		suffix is returned instead.
		
		This method allows strings to be used to refer to roles, relations, etc.
		without direct access to the constants. It also supports the future expansion
		of roles, relations, etc. by allowing arbitrary strings which may or may not
		map to the current standard set of roles, relations, etc., but may still
		match some non-standard role, relation, etc. being reported by an
		application.
		
		@param prefix: Prefix of the constant name such as role, relation, state, 
		        text, modifier, key
		@type prefix: string
		@param suffix: Name of the role, relation, etc. to use to lookup the constant
		@type suffix: string
		@return: The matching constant value
		@rtype: object
	**/
	static public function stringToConst(prefix:Dynamic, suffix:Dynamic):Dynamic;
}