/* This file is generated, do not edit! */
package pyatspi.action;
@:pythonImport("pyatspi.action", "Action") extern class Action {
	public function __class__(args:haxe.extern.Rest<Dynamic>):Dynamic;
	/**
		Implement delattr(self, name).
	**/
	public function __delattr__(name:Dynamic):Dynamic;
	static public var __dict__ : Dynamic;
	/**
		Default dir() implementation.
	**/
	public function __dir__():Dynamic;
	static public var __doc__ : Dynamic;
	/**
		Return self==value.
	**/
	static public function __eq__(a:Dynamic, b:Dynamic):Dynamic;
	/**
		Default object formatter.
	**/
	public function __format__(format_spec:Dynamic):Dynamic;
	/**
		Return self>=value.
	**/
	public function __ge__(value:Dynamic):Dynamic;
	/**
		Return getattr(self, name).
	**/
	public function __getattribute__(name:Dynamic):Dynamic;
	/**
		Return self>value.
	**/
	public function __gt__(value:Dynamic):Dynamic;
	static public var __hash__ : Dynamic;
	/**
		Initialize self.  See help(type(self)) for accurate signature.
	**/
	@:native("__init__")
	public function ___init__(obj:Dynamic):Dynamic;
	/**
		Initialize self.  See help(type(self)) for accurate signature.
	**/
	public function new(obj:Dynamic):Void;
	/**
		This method is called when a class is subclassed.
		
		The default implementation does nothing. It may be
		overridden to extend subclasses.
	**/
	public function __init_subclass__(args:haxe.extern.Rest<Dynamic>):Dynamic;
	/**
		Return self<=value.
	**/
	public function __le__(value:Dynamic):Dynamic;
	/**
		Return self<value.
	**/
	public function __lt__(value:Dynamic):Dynamic;
	static public var __module__ : Dynamic;
	/**
		Return self!=value.
	**/
	public function __ne__(value:Dynamic):Dynamic;
	/**
		Create and return a new object.  See help(type) for accurate signature.
	**/
	static public function __new__(?args:python.VarArgs<Dynamic>, ?kwargs:python.KwArgs<Dynamic>):Dynamic;
	/**
		Helper for pickle.
	**/
	public function __reduce__():Dynamic;
	/**
		Helper for pickle.
	**/
	public function __reduce_ex__(protocol:Dynamic):Dynamic;
	/**
		Return repr(self).
	**/
	public function __repr__():Dynamic;
	/**
		Implement setattr(self, name, value).
	**/
	public function __setattr__(name:Dynamic, value:Dynamic):Dynamic;
	/**
		Size of object in memory, in bytes.
	**/
	public function __sizeof__():Dynamic;
	/**
		Return str(self).
	**/
	public function __str__():Dynamic;
	/**
		Abstract classes can override this to customize issubclass().
		
		This is invoked early on by abc.ABCMeta.__subclasscheck__().
		It should return True, False or NotImplemented.  If it returns
		NotImplemented, the normal algorithm is used.  Otherwise, it
		overrides the normal algorithm (and the outcome is cached).
	**/
	public function __subclasshook__(args:haxe.extern.Rest<Dynamic>):Dynamic;
	/**
		list of weak references to the object (if defined)
	**/
	public var __weakref__ : Dynamic;
	static public var _nActionsDoc : Dynamic;
	/**
		doAction: 
		@param : index
		the 0-based index of the action to perform.
		Causes the object to perform the specified action.
		@return : a boolean indicating success or failure.
	**/
	public function doAction(index:Dynamic):Bool;
	/**
		getActions:
		Retrieves all the actions at once.  
		@return : an array of an array of strings in the form
		[[name, description, keybinding], ...]
	**/
	public function getActions():Dynamic;
	/**
		getDescription: 
		@param : index
		the index of the action for which a description is desired.
		Get the description of the specified action. The description
		of an action may provide information about the result of action
		invocation, unlike the action name. 
		@return : a string containing the description of the specified
		action.
	**/
	public function getDescription(index:Dynamic):String;
	/**
		getKeyBinding: 
		@param : index
		the 0-based index of the action for which a key binding is requested.
		Get the key binding associated with a specific action.
		@return : a string containing the key binding for the specified
		action, or an empty string ("") if none exists.
	**/
	public function getKeyBinding(index:Dynamic):String;
	/**
		getLocalizedName: 
		@param : index
		the index of the action whose name is requested.
		Get the localized name of the specified action. Action names
		generally describe the user action, i.e. "click" or "press",
		rather than the result of invoking the action.
		@return : a string containing the name of the specified action.
	**/
	public function getLocalizedName(index:Dynamic):String;
	/**
		getName: 
		@param : index
		the index of the action whose name is requested.
		Get the unlocalized name of the specified action. Action names
		generally describe the user action, i.e. "click" or "press",
		rather than the result of invoking the action.
		@return : a string containing the name of the specified action.
	**/
	public function getName(index:Int):String;
	public function get_nActions():Int;
	/**
		nActions: a long containing the number of actions this object
		supports.
	**/
	public var nActions : Dynamic;
	static public function queryAction(x:Dynamic):Dynamic;
	static public function queryCollection(x:Dynamic):Dynamic;
	static public function queryComponent(x:Dynamic):Dynamic;
	static public function queryDocument(x:Dynamic):Dynamic;
	static public function queryEditableText(x:Dynamic):Dynamic;
	static public function queryHyperlink(x:Dynamic):Dynamic;
	static public function queryHypertext(x:Dynamic):Dynamic;
	static public function queryImage(x:Dynamic):Dynamic;
	static public function querySelection(x:Dynamic):Dynamic;
	static public function queryTable(x:Dynamic):Dynamic;
	static public function queryTableCell(x:Dynamic):Dynamic;
	static public function queryText(x:Dynamic):Dynamic;
	static public function queryValue(x:Dynamic):Dynamic;
}