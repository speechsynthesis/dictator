package pyatspi;

@:pythonImport("pyatspi","Registry")
extern class Registry {

    //pyatspi.Registry.registerEventListener(on_caret_move, "object:text-caret-moved")
    @:native("registerEventListener")
    public static function registerEventListener(func:Dynamic,  type:String ):Void;

    //pyatspi.Registry.registerKeystrokeListener(on_key_input, kind=(pyatspi.KEY_PRESSED_EVENT,pyatspi.KEY_RELEASED_EVENT))
    @:native("registerEventListener")
    public static function registerKeystrokeListener(func:Dynamic,  kind:Dynamic ):Void;

    @:native("start")
    public static function start():Void;

}
