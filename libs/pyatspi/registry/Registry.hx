/* This file is generated, do not edit! */
package pyatspi.registry;
@:pythonImport("pyatspi.registry", "Registry") extern class Registry {
	static public var _KEY_PRESSED_EVENT : Dynamic;
	static public var _KEY_RELEASED_EVENT : Dynamic;
	static public var _Registry__shared_state : Dynamic;
	/**
		@@return: This instance of the registry
		@@rtype: L{Registry}
	**/
	public function __call__():Dynamic;
	public function __class__(args:haxe.extern.Rest<Dynamic>):Dynamic;
	/**
		Implement delattr(self, name).
	**/
	public function __delattr__(name:Dynamic):Dynamic;
	static public var __dict__ : Dynamic;
	/**
		Default dir() implementation.
	**/
	public function __dir__():Dynamic;
	static public var __doc__ : Dynamic;
	/**
		Return self==value.
	**/
	public function __eq__(value:Dynamic):Dynamic;
	/**
		Default object formatter.
	**/
	public function __format__(format_spec:Dynamic):Dynamic;
	/**
		Return self>=value.
	**/
	public function __ge__(value:Dynamic):Dynamic;
	/**
		For backwards compatibility with old API
	**/
	public function __getattr__(name:Dynamic):Dynamic;
	/**
		Return getattr(self, name).
	**/
	public function __getattribute__(name:Dynamic):Dynamic;
	/**
		Return self>value.
	**/
	public function __gt__(value:Dynamic):Dynamic;
	/**
		Return hash(self).
	**/
	public function __hash__():Dynamic;
	/**
		Initialize self.  See help(type(self)) for accurate signature.
	**/
	@:native("__init__")
	public function ___init__():Dynamic;
	/**
		Initialize self.  See help(type(self)) for accurate signature.
	**/
	public function new():Void;
	/**
		This method is called when a class is subclassed.
		
		The default implementation does nothing. It may be
		overridden to extend subclasses.
	**/
	public function __init_subclass__(args:haxe.extern.Rest<Dynamic>):Dynamic;
	/**
		Return self<=value.
	**/
	public function __le__(value:Dynamic):Dynamic;
	/**
		Return self<value.
	**/
	public function __lt__(value:Dynamic):Dynamic;
	static public var __module__ : Dynamic;
	/**
		Return self!=value.
	**/
	public function __ne__(value:Dynamic):Dynamic;
	/**
		Create and return a new object.  See help(type) for accurate signature.
	**/
	static public function __new__(?args:python.VarArgs<Dynamic>, ?kwargs:python.KwArgs<Dynamic>):Dynamic;
	/**
		Helper for pickle.
	**/
	public function __reduce__():Dynamic;
	/**
		Helper for pickle.
	**/
	public function __reduce_ex__(protocol:Dynamic):Dynamic;
	/**
		Return repr(self).
	**/
	public function __repr__():Dynamic;
	/**
		For backwards compatibility with old API
	**/
	public function __setattr__(name:Dynamic, value:Dynamic):Dynamic;
	/**
		Size of object in memory, in bytes.
	**/
	public function __sizeof__():Dynamic;
	/**
		Return str(self).
	**/
	public function __str__():Dynamic;
	/**
		Abstract classes can override this to customize issubclass().
		
		This is invoked early on by abc.ABCMeta.__subclasscheck__().
		It should return True, False or NotImplemented.  If it returns
		NotImplemented, the normal algorithm is used.  Otherwise, it
		overrides the normal algorithm (and the outcome is cached).
	**/
	public function __subclasshook__(args:haxe.extern.Rest<Dynamic>):Dynamic;
	/**
		list of weak references to the object (if defined)
	**/
	public var __weakref__ : Dynamic;
	public function _set_default_registry():Dynamic;
	/**
		Creates a new 'Registry' object and sets this object
		as the default returned by pyatspi.Registry.
		
		The default registry (without calling this function) uses the
		GLib main loop with caching. It connects to a registry daemon.
		
		This function should be called before pyatspi is used if you
		wish to change these defaults.
		
		@@param main_loop_type: 'GLib', 'None' or 'Qt'. If 'None' is selected then caching
		                       is disabled.
		
		@@param use_registry: Whether to connect to a registry daemon for device events.
		                     Without this the application to connect to must be declared in the
		                     app_name parameter.
		
		@@param app_name: D-Bus name of the application to connect to when not using the registry daemon.
	**/
	public function _set_registry(main_loop_type:Dynamic, ?app_name:Dynamic):Dynamic;
	/**
		Unregisters an existing client callback for the given event names. Supports 
		unregistration for all subevents if only partial event name is specified.
		Do not include a trailing colon.
		
		This method must be called to ensure a client registered by
		L{registerEventListener} is properly garbage collected.
		
		@@param client: Client callback to remove
		@@type client: callable
		@@param names: List of full or partial event names
		@@type names: list of string
		@@return: Were event names specified for which the given client was not
		        registered?
		@@rtype: boolean
	**/
	public function deregisterEventListener(client:Dynamic, ?names:python.VarArgs<Dynamic>):Dynamic;
	/**
		Deregisters a listener for key stroke events.
		
		@@param client: Callable to be invoked when the event occurs
		@@type client: callable
		@@param key_set: Set of hardware key codes to stop monitoring. Leave empty
		        to indicate all keys.
		@@type key_set: list of integer
		@@param mask: When the mask is None, the codes in the key_set will be 
		        monitored only when no modifier is held. When the mask is an 
		        integer, keys in the key_set will be monitored only when the modifiers in
		        the mask are held. When the mask is an iterable over more than one 
		        integer, keys in the key_set will be monitored when any of the modifier
		        combinations in the set are held.
		@@type mask: integer, iterable, None
		@@param kind: Kind of events to stop watching, KEY_PRESSED_EVENT or 
		        KEY_RELEASED_EVENT.
		@@type kind: list
		@@raise KeyError: When the client isn't already registered for events
	**/
	public function deregisterKeystrokeListener(client:Dynamic, ?key_set:Dynamic, ?mask:Dynamic, ?kind:Dynamic):Dynamic;
	public function eventWrapper(event:Dynamic, callback:Dynamic):Dynamic;
	/**
		Generates a keyboard event. One of the keycode or the keysym parameters
		should be specified and the other should be None. The kind parameter is 
		required and should be one of the KEY_PRESS, KEY_RELEASE, KEY_PRESSRELEASE,
		KEY_SYM, KEY_STRING, KEY_LOCKMODIFIERS, or KEY_UNLOCKMODIFIERS.
		
		@@param keycode: Hardware keycode or None
		@@type keycode: integer
		@@param keysym: Symbolic key string or None
		@@type keysym: string
		@@param kind: Kind of event to synthesize
		@@type kind: integer
	**/
	public function generateKeyboardEvent(keycode:Dynamic, keysym:Dynamic, kind:Dynamic):Dynamic;
	/**
		Generates a mouse event at the given absolute x and y coordinate. The kind
		of event generated is specified by the name. For example, MOUSE_B1P 
		(button 1 press), MOUSE_REL (relative motion), MOUSE_B3D (butten 3 
		double-click).
		
		@@param x: Horizontal coordinate, usually left-hand oriented
		@@type x: integer
		@@param y: Vertical coordinate, usually left-hand oriented
		@@type y: integer
		@@param name: Name of the event to generate
		@@type name: string
	**/
	public function generateMouseEvent(x:Dynamic, y:Dynamic, name:Dynamic):Dynamic;
	/**
		Gets a reference to the i-th desktop.
		
		@@param i: Which desktop to get
		@@type i: integer
		@@return: Desktop reference
		@@rtype: Accessibility.Desktop
	**/
	public function getDesktop(i:Dynamic):Dynamic;
	/**
		Gets the number of available desktops.
		
		@@return: Number of desktops
		@@rtype: integer
	**/
	public function getDesktopCount():Dynamic;
	public function makeKind(kind:Dynamic):Dynamic;
	public function makeSyncType(synchronous:Dynamic, preemptive:Dynamic, global_:Dynamic):Dynamic;
	/**
		Dispatch events that have been queued.
	**/
	public function pumpQueuedEvents():Dynamic;
	/**
		Registers a new client callback for the given event names. Supports 
		registration for all subevents if only partial event name is specified.
		Do not include a trailing colon.
		
		For example, 'object' will register for all object events, 
		'object:property-change' will register for all property change events,
		and 'object:property-change:accessible-parent' will register only for the
		parent property change event.
		
		Registered clients will not be automatically removed when the client dies.
		To ensure the client is properly garbage collected, call 
		L{deregisterEventListener}.
		
		@@param client: Callable to be invoked when the event occurs
		@@type client: callable
		@@param names: List of full or partial event names
		@@type names: list of string
	**/
	public function registerEventListener(client:Dynamic, ?names:python.VarArgs<Dynamic>):Dynamic;
	/**
		Registers a listener for key stroke events.
		
		@@param client: Callable to be invoked when the event occurs
		@@type client: callable
		@@param key_set: Set of hardware key codes to stop monitoring. Leave empty
		        to indicate all keys.
		@@type key_set: list of integer
		@@param mask: When the mask is None, the codes in the key_set will be 
		        monitored only when no modifier is held. When the mask is an 
		        integer, keys in the key_set will be monitored only when the modifiers in
		        the mask are held. When the mask is an iterable over more than one 
		        integer, keys in the key_set will be monitored when any of the modifier
		        combinations in the set are held.
		@@type mask: integer, iterable, None
		@@param kind: Kind of events to watch, KEY_PRESSED_EVENT or 
		        KEY_RELEASED_EVENT.
		@@type kind: list
		@@param synchronous: Should the callback notification be synchronous, giving
		        the client the chance to consume the event?
		@@type synchronous: boolean
		@@param preemptive: Should the callback be allowed to preempt / consume the
		        event?
		@@type preemptive: boolean
		@@param global_: Should callback occur even if an application not supporting
		        AT-SPI is in the foreground? (requires xevie)
		@@type global_: boolean
	**/
	public function registerKeystrokeListener(client:Dynamic, ?key_set:Dynamic, ?mask:Dynamic, ?kind:Dynamic, ?synchronous:Dynamic, ?preemptive:Dynamic, ?global_:Dynamic):Dynamic;
	/**
		Sets the reference window that will be used when
		generateMouseEvent is called. Coordinates will be assumed to
		be relative to this window. This * is needed because, due to
		Wayland's security model, it is not currently possible to
		possible to retrieve global coordinates.
		If NULL is passed, then AT-SPI will use the window that has
		focus at the time that atspi_generate_mouse_event is called.
		
		@@param accessible: the accessible corresponding to the window
		to select. should be a top-level window with a role of
		pyatspi.ROLE_APPLICATION.
	**/
	static public function setReferenceWIndow(accessible:Dynamic):Dynamic;
	/**
		Enter the main loop to start receiving and dispatching events.
		
		@@param asynchronous: Should event dispatch be asynchronous
		        (decoupled) from event receiving from the AT-SPI registry?
		@@type asynchronous: boolean
		@@param gil: Add an idle callback which releases the Python GIL for a few
		        milliseconds to allow other threads to run? Necessary if other threads
		        will be used in this process.
		@@type gil: boolean
	**/
	public function start(?asynchronous:Dynamic, ?gil:Dynamic, ?kwargs:python.KwArgs<Dynamic>):Dynamic;
	/**
		Quits the main loop.
	**/
	public function stop(?args:python.VarArgs<Dynamic>):Dynamic;
}