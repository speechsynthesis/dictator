/* This file is generated, do not edit! */
package pyatspi.registry;
@:pythonImport("pyatspi.registry") extern class Registry_Module {
	static public var Atspi : Dynamic;
	static public var MAIN_LOOP_GLIB : Dynamic;
	static public var MAIN_LOOP_NONE : Dynamic;
	static public var MAIN_LOOP_QT : Dynamic;
	static public var __all__ : Dynamic;
	static public var __builtins__ : Dynamic;
	static public var __cached__ : Dynamic;
	static public var __doc__ : Dynamic;
	static public var __file__ : Dynamic;
	static public var __loader__ : Dynamic;
	static public var __name__ : Dynamic;
	static public var __package__ : Dynamic;
	static public var __spec__ : Dynamic;
	static public function set_default_registry(main_loop:Dynamic, ?app_name:Dynamic):Dynamic;
}