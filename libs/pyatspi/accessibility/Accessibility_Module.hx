/* This file is generated, do not edit! */
package pyatspi.accessibility;
@:pythonImport("pyatspi.Accessibility") extern class Accessibility_Module {
	static public function Accessible_getitem(self:Dynamic, i:Dynamic):Dynamic;
	/**
		Gets a human readable representation of the accessible.
		
		@return: Role and name information for the accessible
		@rtype: string
	**/
	static public function Accessible_str(self:Dynamic):Dynamic;
	static public var Atspi : Dynamic;
	static public var BUTTON_PRESSED_EVENT : Dynamic;
	static public var BUTTON_RELEASED_EVENT : Dynamic;
	static public var CACHE_EVENTS : Dynamic;
	static public var CACHE_PROPERTIES : Dynamic;
	static public var DESKTOP_COORDS : Dynamic;
	/**
		Builds a human readable representation of the event.
		
		@return: Event description
		@rtype: string
	**/
	static public function DeviceEvent_str(self:Dynamic):Dynamic;
	static public var EVENT_TREE : Dynamic;
	/**
		Builds a human readable representation of the event including event type,
		parameters, and source info.
		
		@return: Event description
		@rtype: string
	**/
	static public function Event_str(self:Dynamic):Dynamic;
	static public var KEY_LOCKMODIFIERS : Dynamic;
	static public var KEY_PRESS : Dynamic;
	static public var KEY_PRESSED_EVENT : Dynamic;
	static public var KEY_PRESSRELEASE : Dynamic;
	static public var KEY_RELEASE : Dynamic;
	static public var KEY_RELEASED_EVENT : Dynamic;
	static public var KEY_STRING : Dynamic;
	static public var KEY_SYM : Dynamic;
	static public var KEY_UNLOCKMODIFIERS : Dynamic;
	static public var LAYER_BACKGROUND : Dynamic;
	static public var LAYER_CANVAS : Dynamic;
	static public var LAYER_INVALID : Dynamic;
	static public var LAYER_LAST_DEFINED : Dynamic;
	static public var LAYER_MDI : Dynamic;
	static public var LAYER_OVERLAY : Dynamic;
	static public var LAYER_POPUP : Dynamic;
	static public var LAYER_WIDGET : Dynamic;
	static public var LAYER_WINDOW : Dynamic;
	static public var MAIN_LOOP_GLIB : Dynamic;
	static public var MAIN_LOOP_NONE : Dynamic;
	static public var MODIFIER_ALT : Dynamic;
	static public var MODIFIER_CONTROL : Dynamic;
	static public var MODIFIER_META : Dynamic;
	static public var MODIFIER_META2 : Dynamic;
	static public var MODIFIER_META3 : Dynamic;
	static public var MODIFIER_NUMLOCK : Dynamic;
	static public var MODIFIER_SHIFT : Dynamic;
	static public var MODIFIER_SHIFTLOCK : Dynamic;
	static public var MOUSE_ABS : Dynamic;
	static public var MOUSE_B1C : Dynamic;
	static public var MOUSE_B1D : Dynamic;
	static public var MOUSE_B1P : Dynamic;
	static public var MOUSE_B1R : Dynamic;
	static public var MOUSE_B2C : Dynamic;
	static public var MOUSE_B2D : Dynamic;
	static public var MOUSE_B2P : Dynamic;
	static public var MOUSE_B2R : Dynamic;
	static public var MOUSE_B3C : Dynamic;
	static public var MOUSE_B3D : Dynamic;
	static public var MOUSE_B3P : Dynamic;
	static public var MOUSE_B3R : Dynamic;
	static public var MOUSE_REL : Dynamic;
	static public var RELATION_CONTROLLED_BY : Dynamic;
	static public var RELATION_CONTROLLER_FOR : Dynamic;
	static public var RELATION_DESCRIBED_BY : Dynamic;
	static public var RELATION_DESCRIPTION_FOR : Dynamic;
	static public var RELATION_DETAILS : Dynamic;
	static public var RELATION_DETAILS_FOR : Dynamic;
	static public var RELATION_EMBEDDED_BY : Dynamic;
	static public var RELATION_EMBEDS : Dynamic;
	static public var RELATION_ERROR_FOR : Dynamic;
	static public var RELATION_ERROR_MESSAGE : Dynamic;
	static public var RELATION_EXTENDED : Dynamic;
	static public var RELATION_FLOWS_FROM : Dynamic;
	static public var RELATION_FLOWS_TO : Dynamic;
	static public var RELATION_LABELLED_BY : Dynamic;
	static public var RELATION_LABEL_FOR : Dynamic;
	static public var RELATION_MEMBER_OF : Dynamic;
	static public var RELATION_NODE_CHILD_OF : Dynamic;
	static public var RELATION_NODE_PARENT_OF : Dynamic;
	static public var RELATION_NULL : Dynamic;
	static public var RELATION_PARENT_WINDOW_OF : Dynamic;
	static public var RELATION_POPUP_FOR : Dynamic;
	static public var RELATION_SUBWINDOW_OF : Dynamic;
	static public var RELATION_TOOLTIP_FOR : Dynamic;
	static public var RELATION_VALUE_TO_NAME : Dynamic;
	static public var ROLE_ACCELERATOR_LABEL : Dynamic;
	static public var ROLE_ALERT : Dynamic;
	static public var ROLE_ANIMATION : Dynamic;
	static public var ROLE_APPLICATION : Dynamic;
	static public var ROLE_ARROW : Dynamic;
	static public var ROLE_ARTICLE : Dynamic;
	static public var ROLE_AUDIO : Dynamic;
	static public var ROLE_AUTOCOMPLETE : Dynamic;
	static public var ROLE_BLOCK_QUOTE : Dynamic;
	static public var ROLE_CALENDAR : Dynamic;
	static public var ROLE_CANVAS : Dynamic;
	static public var ROLE_CAPTION : Dynamic;
	static public var ROLE_CHART : Dynamic;
	static public var ROLE_CHECK_BOX : Dynamic;
	static public var ROLE_CHECK_MENU_ITEM : Dynamic;
	static public var ROLE_COLOR_CHOOSER : Dynamic;
	static public var ROLE_COLUMN_HEADER : Dynamic;
	static public var ROLE_COMBO_BOX : Dynamic;
	static public var ROLE_COMMENT : Dynamic;
	static public var ROLE_CONTENT_DELETION : Dynamic;
	static public var ROLE_CONTENT_INSERTION : Dynamic;
	static public var ROLE_DATE_EDITOR : Dynamic;
	static public var ROLE_DEFINITION : Dynamic;
	static public var ROLE_DESCRIPTION_LIST : Dynamic;
	static public var ROLE_DESCRIPTION_TERM : Dynamic;
	static public var ROLE_DESCRIPTION_VALUE : Dynamic;
	static public var ROLE_DESKTOP_FRAME : Dynamic;
	static public var ROLE_DESKTOP_ICON : Dynamic;
	static public var ROLE_DIAL : Dynamic;
	static public var ROLE_DIALOG : Dynamic;
	static public var ROLE_DIRECTORY_PANE : Dynamic;
	static public var ROLE_DOCUMENT_EMAIL : Dynamic;
	static public var ROLE_DOCUMENT_FRAME : Dynamic;
	static public var ROLE_DOCUMENT_PRESENTATION : Dynamic;
	static public var ROLE_DOCUMENT_SPREADSHEET : Dynamic;
	static public var ROLE_DOCUMENT_TEXT : Dynamic;
	static public var ROLE_DOCUMENT_WEB : Dynamic;
	static public var ROLE_DRAWING_AREA : Dynamic;
	static public var ROLE_EDITBAR : Dynamic;
	static public var ROLE_EMBEDDED : Dynamic;
	static public var ROLE_ENTRY : Dynamic;
	static public var ROLE_EXTENDED : Dynamic;
	static public var ROLE_FILE_CHOOSER : Dynamic;
	static public var ROLE_FILLER : Dynamic;
	static public var ROLE_FOCUS_TRAVERSABLE : Dynamic;
	static public var ROLE_FONT_CHOOSER : Dynamic;
	static public var ROLE_FOOTER : Dynamic;
	static public var ROLE_FOOTNOTE : Dynamic;
	static public var ROLE_FORM : Dynamic;
	static public var ROLE_FRAME : Dynamic;
	static public var ROLE_GLASS_PANE : Dynamic;
	static public var ROLE_GROUPING : Dynamic;
	static public var ROLE_HEADER : Dynamic;
	static public var ROLE_HEADING : Dynamic;
	static public var ROLE_HTML_CONTAINER : Dynamic;
	static public var ROLE_ICON : Dynamic;
	static public var ROLE_IMAGE : Dynamic;
	static public var ROLE_IMAGE_MAP : Dynamic;
	static public var ROLE_INFO_BAR : Dynamic;
	static public var ROLE_INPUT_METHOD_WINDOW : Dynamic;
	static public var ROLE_INTERNAL_FRAME : Dynamic;
	static public var ROLE_INVALID : Dynamic;
	static public var ROLE_LABEL : Dynamic;
	static public var ROLE_LANDMARK : Dynamic;
	static public var ROLE_LAST_DEFINED : Dynamic;
	static public var ROLE_LAYERED_PANE : Dynamic;
	static public var ROLE_LEVEL_BAR : Dynamic;
	static public var ROLE_LINK : Dynamic;
	static public var ROLE_LIST : Dynamic;
	static public var ROLE_LIST_BOX : Dynamic;
	static public var ROLE_LIST_ITEM : Dynamic;
	static public var ROLE_LOG : Dynamic;
	static public var ROLE_MARK : Dynamic;
	static public var ROLE_MARQUEE : Dynamic;
	static public var ROLE_MATH : Dynamic;
	static public var ROLE_MATH_FRACTION : Dynamic;
	static public var ROLE_MATH_ROOT : Dynamic;
	static public var ROLE_MENU : Dynamic;
	static public var ROLE_MENU_BAR : Dynamic;
	static public var ROLE_MENU_ITEM : Dynamic;
	static public var ROLE_NAMES : Dynamic;
	static public var ROLE_NOTIFICATION : Dynamic;
	static public var ROLE_OPTION_PANE : Dynamic;
	static public var ROLE_PAGE : Dynamic;
	static public var ROLE_PAGE_TAB : Dynamic;
	static public var ROLE_PAGE_TAB_LIST : Dynamic;
	static public var ROLE_PANEL : Dynamic;
	static public var ROLE_PARAGRAPH : Dynamic;
	static public var ROLE_PASSWORD_TEXT : Dynamic;
	static public var ROLE_POPUP_MENU : Dynamic;
	static public var ROLE_PROGRESS_BAR : Dynamic;
	static public var ROLE_PUSH_BUTTON : Dynamic;
	static public var ROLE_RADIO_BUTTON : Dynamic;
	static public var ROLE_RADIO_MENU_ITEM : Dynamic;
	static public var ROLE_RATING : Dynamic;
	static public var ROLE_REDUNDANT_OBJECT : Dynamic;
	static public var ROLE_ROOT_PANE : Dynamic;
	static public var ROLE_ROW_HEADER : Dynamic;
	static public var ROLE_RULER : Dynamic;
	static public var ROLE_SCROLL_BAR : Dynamic;
	static public var ROLE_SCROLL_PANE : Dynamic;
	static public var ROLE_SECTION : Dynamic;
	static public var ROLE_SEPARATOR : Dynamic;
	static public var ROLE_SLIDER : Dynamic;
	static public var ROLE_SPIN_BUTTON : Dynamic;
	static public var ROLE_SPLIT_PANE : Dynamic;
	static public var ROLE_STATIC : Dynamic;
	static public var ROLE_STATUS_BAR : Dynamic;
	static public var ROLE_SUBSCRIPT : Dynamic;
	static public var ROLE_SUGGESTION : Dynamic;
	static public var ROLE_SUPERSCRIPT : Dynamic;
	static public var ROLE_TABLE : Dynamic;
	static public var ROLE_TABLE_CELL : Dynamic;
	static public var ROLE_TABLE_COLUMN_HEADER : Dynamic;
	static public var ROLE_TABLE_ROW : Dynamic;
	static public var ROLE_TABLE_ROW_HEADER : Dynamic;
	static public var ROLE_TEAROFF_MENU_ITEM : Dynamic;
	static public var ROLE_TERMINAL : Dynamic;
	static public var ROLE_TEXT : Dynamic;
	static public var ROLE_TIMER : Dynamic;
	static public var ROLE_TITLE_BAR : Dynamic;
	static public var ROLE_TOGGLE_BUTTON : Dynamic;
	static public var ROLE_TOOL_BAR : Dynamic;
	static public var ROLE_TOOL_TIP : Dynamic;
	static public var ROLE_TREE : Dynamic;
	static public var ROLE_TREE_ITEM : Dynamic;
	static public var ROLE_TREE_TABLE : Dynamic;
	static public var ROLE_UNKNOWN : Dynamic;
	static public var ROLE_VIDEO : Dynamic;
	static public var ROLE_VIEWPORT : Dynamic;
	static public var ROLE_WINDOW : Dynamic;
	static public function Registry():Dynamic;
	static public var SCROLL_ANYWHERE : Dynamic;
	static public var SCROLL_BOTTOM_EDGE : Dynamic;
	static public var SCROLL_BOTTOM_RIGHT : Dynamic;
	static public var SCROLL_LEFT_EDGE : Dynamic;
	static public var SCROLL_RIGHT_EDGE : Dynamic;
	static public var SCROLL_TOP_EDGE : Dynamic;
	static public var SCROLL_TOP_LEFT : Dynamic;
	static public var STATE_ACTIVE : Dynamic;
	static public var STATE_ANIMATED : Dynamic;
	static public var STATE_ARMED : Dynamic;
	static public var STATE_BUSY : Dynamic;
	static public var STATE_CHECKABLE : Dynamic;
	static public var STATE_CHECKED : Dynamic;
	static public var STATE_COLLAPSED : Dynamic;
	static public var STATE_DEFUNCT : Dynamic;
	static public var STATE_EDITABLE : Dynamic;
	static public var STATE_ENABLED : Dynamic;
	static public var STATE_EXPANDABLE : Dynamic;
	static public var STATE_EXPANDED : Dynamic;
	static public var STATE_FOCUSABLE : Dynamic;
	static public var STATE_FOCUSED : Dynamic;
	static public var STATE_HAS_POPUP : Dynamic;
	static public var STATE_HAS_TOOLTIP : Dynamic;
	static public var STATE_HORIZONTAL : Dynamic;
	static public var STATE_ICONIFIED : Dynamic;
	static public var STATE_INDETERMINATE : Dynamic;
	static public var STATE_INVALID : Dynamic;
	static public var STATE_INVALID_ENTRY : Dynamic;
	static public var STATE_IS_DEFAULT : Dynamic;
	static public var STATE_LAST_DEFINED : Dynamic;
	static public var STATE_MANAGES_DESCENDANTS : Dynamic;
	static public var STATE_MODAL : Dynamic;
	static public var STATE_MULTISELECTABLE : Dynamic;
	static public var STATE_MULTI_LINE : Dynamic;
	static public var STATE_OPAQUE : Dynamic;
	static public var STATE_PRESSED : Dynamic;
	static public var STATE_READ_ONLY : Dynamic;
	static public var STATE_REQUIRED : Dynamic;
	static public var STATE_RESIZABLE : Dynamic;
	static public var STATE_SELECTABLE : Dynamic;
	static public var STATE_SELECTABLE_TEXT : Dynamic;
	static public var STATE_SELECTED : Dynamic;
	static public var STATE_SENSITIVE : Dynamic;
	static public var STATE_SHOWING : Dynamic;
	static public var STATE_SINGLE_LINE : Dynamic;
	static public var STATE_STALE : Dynamic;
	static public var STATE_SUPPORTS_AUTOCOMPLETION : Dynamic;
	static public var STATE_TRANSIENT : Dynamic;
	static public var STATE_TRUNCATED : Dynamic;
	static public var STATE_VALUE_TO_NAME : Dynamic;
	static public var STATE_VERTICAL : Dynamic;
	static public var STATE_VISIBLE : Dynamic;
	static public var STATE_VISITED : Dynamic;
	static public function StateSet_getStates(self:Dynamic):Dynamic;
	static public var TEXT_BOUNDARY_CHAR : Dynamic;
	static public var TEXT_BOUNDARY_LINE_END : Dynamic;
	static public var TEXT_BOUNDARY_LINE_START : Dynamic;
	static public var TEXT_BOUNDARY_SENTENCE_END : Dynamic;
	static public var TEXT_BOUNDARY_SENTENCE_START : Dynamic;
	static public var TEXT_BOUNDARY_WORD_END : Dynamic;
	static public var TEXT_BOUNDARY_WORD_START : Dynamic;
	static public var TEXT_CLIP_BOTH : Dynamic;
	static public var TEXT_CLIP_MAX : Dynamic;
	static public var TEXT_CLIP_MIN : Dynamic;
	static public var TEXT_CLIP_NONE : Dynamic;
	static public var TEXT_GRANULARITY_CHAR : Dynamic;
	static public var TEXT_GRANULARITY_LINE : Dynamic;
	static public var TEXT_GRANULARITY_PARAGRAPH : Dynamic;
	static public var TEXT_GRANULARITY_SENTENCE : Dynamic;
	static public var TEXT_GRANULARITY_WORD : Dynamic;
	static public var WINDOW_COORDS : Dynamic;
	static public var XY_PARENT : Dynamic;
	static public var XY_SCREEN : Dynamic;
	static public var XY_WINDOW : Dynamic;
	static public var __builtins__ : Dynamic;
	static public var __cached__ : Dynamic;
	static public var __doc__ : Dynamic;
	static public var __file__ : Dynamic;
	static public var __loader__ : Dynamic;
	static public var __name__ : Dynamic;
	static public var __package__ : Dynamic;
	static public var __spec__ : Dynamic;
	/**
		Generates all possible keyboard modifiers for use with 
		L{registry.Registry.registerKeystrokeListener}.
	**/
	static public function allModifiers():Dynamic;
	static public function attributeListToHash(list:Dynamic):Dynamic;
	static public function clearCache():Dynamic;
	/**
		Searches for all descendant nodes satisfying the given predicate starting at 
		this node. Does an in-order traversal. For example,
		
		pred = lambda x: x.getRole() == pyatspi.ROLE_PUSH_BUTTON
		buttons = pyatspi.findAllDescendants(node, pred)
		
		will locate all push button descendants of node.
		
		@param acc: Root accessible of the search
		@type acc: Accessibility.Accessible
		@param pred: Search predicate returning True if accessible matches the 
		                search criteria or False otherwise
		@type pred: callable
		@return: All nodes matching the search criteria
		@rtype: list
	**/
	static public function findAllDescendants(acc:Dynamic, pred:Dynamic):Dynamic;
	/**
		Searches for an ancestor satisfying the given predicate. Note that the
		AT-SPI hierarchy is not always doubly linked. Node A may consider node B its
		child, but B is not guaranteed to have node A as its parent (i.e. its parent
		may be set to None). This means some searches may never make it all the way
		up the hierarchy to the desktop level.
		
		@param acc: Starting accessible object
		@type acc: Accessibility.Accessible
		@param pred: Search predicate returning True if accessible matches the 
		        search criteria or False otherwise
		@type pred: callable
		@return: Node matching the criteria or None if not found
		@rtype: Accessibility.Accessible
	**/
	static public function findAncestor(acc:Dynamic, pred:Dynamic):Dynamic;
	/**
		Searches for a descendant node satisfying the given predicate starting at 
		this node. The search is performed in depth-first order by default or
		in breadth first order if breadth_first is True. For example,
		
		my_win = findDescendant(lambda x: x.name == 'My Window')
		
		will search all descendants of x until one is located with the name 'My
		Window' or all nodes are exausted. Calls L{_findDescendantDepth} or
		L{_findDescendantBreadth} to start the recursive search.
		
		@param acc: Root accessible of the search
		@type acc: Accessibility.Accessible
		@param pred: Search predicate returning True if accessible matches the 
		                search criteria or False otherwise
		@type pred: callable
		@param breadth_first: Search breadth first (True) or depth first (False)?
		@type breadth_first: boolean
		@return: Accessible matching the criteria or None if not found
		@rtype: Accessibility.Accessible or None
	**/
	static public function findDescendant(acc:Dynamic, pred:Dynamic, ?breadth_first:Dynamic):Dynamic;
	static public function getBoundingBox(rect:Dynamic):Dynamic;
	static public function getCacheLevel():Dynamic;
	static public function getEventType(event:Dynamic):Dynamic;
	static public function getInterface(func:Dynamic, obj:Dynamic):Dynamic;
	/**
		Gets the ID of an interface class or object in string format for use in
		queryInterface.
		
		@param obj: Class representing an AT-SPI interface or instance
		@type obj: object
		@return: IID for the interface
		@rtype: string
		@raise AttributeError: When the parameter does not provide typecode info
		
		WARNING!! DEPRECATED!!
		
		In current D-Bus version of pyatspi this simply returns a null string.
	**/
	static public function getInterfaceIID(obj:Dynamic):Dynamic;
	/**
		Gets the human readable name of an interface class or object in string
		format.
		
		@param obj: Class representing an AT-SPI interface or instance
		@type obj: class
		@return: Name of the interface
		@rtype: string
		@raise AttributeError: When the parameter does not provide typecode info
	**/
	static public function getInterfaceName(obj:Dynamic):Dynamic;
	/**
		Gets the path from the application ancestor to the given accessible in
		terms of its child index at each level.
		
		@param acc: Target accessible
		@type acc: Accessibility.Accessible
		@return: Path to the target
		@rtype: list of integer
		@raise LookupError: When the application accessible cannot be reached
	**/
	static public function getPath(acc:Dynamic):Dynamic;
	static public function hashToAttributeList(h:Dynamic):Dynamic;
	/**
		Gets a list of the names of all interfaces supported by this object. The
		names are the short-hand interface names like "Accessible" and "Component",
		not the full interface identifiers.
		
		@param obj: Arbitrary object to query for all accessibility related
		interfaces. Must provide a queryInterface method.
		@type obj: object
		@return: Set of supported interface names
		@rtype: set
		@raise AttributeError: If the object provide does not implement
		queryInterface
	**/
	static public function listInterfaces(obj:Dynamic):Dynamic;
	static public function pointToList(point:Dynamic):Dynamic;
	static public function printCache():Dynamic;
	static public function rectToList(rect:Dynamic):Dynamic;
	/**
		Converts a relation value to a string based on the name of the state constant
		in the L{constants} module that has the given value.
		
		@param value: An AT-SPI relation
		@type value: Accessibility.RelationType
		@return: Human readable, untranslated name of the relation
		@rtype: string
	**/
	static public function relationToString(value:Dynamic):Dynamic;
	static public function setCacheLevel(level:Dynamic):Dynamic;
	/**
		set_timeout(val:int, startup_time:int)
	**/
	static public function setTimeout(args:haxe.extern.Rest<Dynamic>):Dynamic;
	static public function set_default_registry(main_loop:Dynamic, ?app_name:Dynamic):Dynamic;
	/**
		Converts a state value to a string based on the name of the state constant in
		the L{constants} module that has the given value.
		
		@param value: An AT-SPI state
		@type value: Accessibility.StateType
		@return: Human readable, untranslated name of the state
		@rtype: string
	**/
	static public function stateToString(value:Dynamic):Dynamic;
	static public function stateset_init(self:Dynamic, ?states:python.VarArgs<Dynamic>):Dynamic;
	/**
		Maps a string name to an AT-SPI constant. The rules for the mapping are as 
		follows:
		        - The prefix is captalized and has an _ appended to it.
		        - All spaces in the suffix are mapped to the _ character. 
		        - All alpha characters in the suffix are mapped to their uppercase.
		
		The resulting name is used with getattr to look up a constant with that name
		in the L{constants} module. If such a constant does not exist, the string
		suffix is returned instead.
		
		This method allows strings to be used to refer to roles, relations, etc.
		without direct access to the constants. It also supports the future expansion
		of roles, relations, etc. by allowing arbitrary strings which may or may not
		map to the current standard set of roles, relations, etc., but may still
		match some non-standard role, relation, etc. being reported by an
		application.
		
		@param prefix: Prefix of the constant name such as role, relation, state, 
		        text, modifier, key
		@type prefix: string
		@param suffix: Name of the role, relation, etc. to use to lookup the constant
		@type suffix: string
		@return: The matching constant value
		@rtype: object
	**/
	static public function stringToConst(prefix:Dynamic, suffix:Dynamic):Dynamic;
}