/* This file is generated, do not edit! */
package pyatspi.editabletext;
@:pythonImport("pyatspi.editabletext", "EditableText") extern class EditableText {
	public function __class__(args:haxe.extern.Rest<Dynamic>):Dynamic;
	/**
		Implement delattr(self, name).
	**/
	public function __delattr__(name:Dynamic):Dynamic;
	static public var __dict__ : Dynamic;
	/**
		Default dir() implementation.
	**/
	public function __dir__():Dynamic;
	static public var __doc__ : Dynamic;
	/**
		Return self==value.
	**/
	static public function __eq__(a:Dynamic, b:Dynamic):Dynamic;
	/**
		Default object formatter.
	**/
	public function __format__(format_spec:Dynamic):Dynamic;
	/**
		Return self>=value.
	**/
	public function __ge__(value:Dynamic):Dynamic;
	/**
		Return getattr(self, name).
	**/
	public function __getattribute__(name:Dynamic):Dynamic;
	/**
		Return self>value.
	**/
	public function __gt__(value:Dynamic):Dynamic;
	static public var __hash__ : Dynamic;
	/**
		Initialize self.  See help(type(self)) for accurate signature.
	**/
	@:native("__init__")
	public function ___init__(obj:Dynamic):Dynamic;
	/**
		Initialize self.  See help(type(self)) for accurate signature.
	**/
	public function new(obj:Dynamic):Void;
	/**
		This method is called when a class is subclassed.
		
		The default implementation does nothing. It may be
		overridden to extend subclasses.
	**/
	public function __init_subclass__(args:haxe.extern.Rest<Dynamic>):Dynamic;
	/**
		Return self<=value.
	**/
	public function __le__(value:Dynamic):Dynamic;
	/**
		Return self<value.
	**/
	public function __lt__(value:Dynamic):Dynamic;
	static public var __module__ : Dynamic;
	/**
		Return self!=value.
	**/
	public function __ne__(value:Dynamic):Dynamic;
	/**
		Create and return a new object.  See help(type) for accurate signature.
	**/
	static public function __new__(?args:python.VarArgs<Dynamic>, ?kwargs:python.KwArgs<Dynamic>):Dynamic;
	/**
		Helper for pickle.
	**/
	public function __reduce__():Dynamic;
	/**
		Helper for pickle.
	**/
	public function __reduce_ex__(protocol:Dynamic):Dynamic;
	/**
		Return repr(self).
	**/
	public function __repr__():Dynamic;
	/**
		Implement setattr(self, name, value).
	**/
	public function __setattr__(name:Dynamic, value:Dynamic):Dynamic;
	/**
		Size of object in memory, in bytes.
	**/
	public function __sizeof__():Dynamic;
	/**
		Return str(self).
	**/
	public function __str__():Dynamic;
	/**
		Abstract classes can override this to customize issubclass().
		
		This is invoked early on by abc.ABCMeta.__subclasscheck__().
		It should return True, False or NotImplemented.  If it returns
		NotImplemented, the normal algorithm is used.  Otherwise, it
		overrides the normal algorithm (and the outcome is cached).
	**/
	public function __subclasshook__(args:haxe.extern.Rest<Dynamic>):Dynamic;
	/**
		list of weak references to the object (if defined)
	**/
	public var __weakref__ : Dynamic;
	static public var _caretOffsetDoc : Dynamic;
	static public var _characterCountDoc : Dynamic;
	/**
		The result of calling addSelection on objects which already have
		one selection present, and which do not include STATE_MULTISELECTABLE,
		is undefined, other than the return value. 
		@return True of the selection was successfully added, False otherwise.
		Selection may fail if the object does not support selection of
		text (see STATE_SELECTABLE_TEXT), if the object does not support
		multiple selections and a selection is already defined, or for
		other reasons (for instance if the user does not have permission
		to copy the text into the relevant selection buffer).
	**/
	public function addSelection(startOffset:Dynamic, endOffset:Dynamic):Dynamic;
	/**
		The current offset of the text caret in the Text object. This
		caret may be virtual, e.g. non-visual and notional-only, but
		if an onscreen representation of the caret position is visible,
		it will correspond to this offset. The caret offset is given
		as a character offset, as opposed to a byte offset into a text
		buffer or a column offset.
	**/
	public var caretOffset : Dynamic;
	/**
		The total current number of characters in the Text object, including
		whitespace and non-spacing characters.
	**/
	public var characterCount : Dynamic;
	/**
		Copy a range of text into the system clipboard. 
		@param : startPos
		the character offset of the first character in the range of text
		being copied. 
		@param : endPos
		the offset of the first character past the end of the range of
		text being copied.
	**/
	public function copyText(start:Dynamic, end:Dynamic):Dynamic;
	/**
		Excise a range of text from a Text object, copying it into the
		system clipboard. 
		@param : startPos
		the character offset of the first character in the range of text
		being cut. 
		@param : endPos
		the offset of the first character past the end of the range of
		text being cut. 
		@return True if the text was successfully cut, False otherwise.
	**/
	public function cutText(start:Dynamic, end:Dynamic):Dynamic;
	/**
		Excise a range of text from a Text object without copying it
		into the system clipboard. 
		@param : startPos
		the character offset of the first character in the range of text
		being deleted. 
		@param : endPos
		the offset of the first character past the end of the range of
		text being deleted. 
		@return True if the text was successfully deleted, False otherwise.
	**/
	public function deleteText(start:Dynamic, end:Dynamic):Dynamic;
	/**
		Query a particular text object for the text attributes defined
		at a given offset, obtaining the start and end of the "attribute
		run" over which these attributes are currently invariant. Text
		attributes are those presentational, typographic, or semantic
		attributes or qualitites which apply to a range of text specifyable
		by starting and ending offsets. Attributes relevant to localization
		should be provided in accordance with the w3c "Internationalization
		and Localization Markup Requirements", http://www.w3.org/TR/2005/WD-itsreq-20051122/
		Other text attributes should choose their names and value semantics
		in accordance with relevant standards such as CSS level 2 (http://www.w3.org/TR/1998/REC-CSS2-19980512),
		XHTML 1.0 (http://www.w3.org/TR/2002/REC-xhtml1-20020801), and
		WICD (http://www.w3.org/TR/2005/WD-WICD-20051121/). Those attributes
		from the aforementioned specifications and recommendations which
		do not concern typographic, presentational, or semantic aspects
		of text should be exposed via the more general Accessible::getAttributes()
		API (if at all).
		For example, CSS attributes which should be exposed on text (either
		as default attributes, or as explicitly-set attributes when non-default
		values are specified in the content view) include the Font attributes
		(i.e. "css2:font-weight", "css2:font-style"), the "css2:color"
		and "css2:background-color" attributes, and "css2:text-decoration"
		attribute.
		If includeDefaults is TRUE, then this AttributeSet should include
		the default attributes as well as those which are explicitly
		assigned to the attribute run in question. startOffset and endOffset
		will be back-filled to indicate the start and end of the attribute
		run which contains 'offset' - an attribute run is a contiguous
		section of text whose attributes are homogeneous. 
		@param : offset
		the offset of the character whose attributes will be reported.
		@param : startOffset
		backfilled with the starting offset of the character range over
		which all text attributes match those of offset, i.e. the start
		of the homogeneous attribute run including offset. 
		@param : endOffset
		backfilled with the offset of the first character past the character
		range over which all text attributes match those of offset, i.e.
		the character immediately after the homogeneous attribute run
		including offset. 
		@param : includeDefaults
		if False, the call should only return those attributes which
		are explicitly set on the current attribute run, omitting any
		attributes which are inherited from the default values. See also
		Text::getDefaultAttributes.
		@return the AttributeSet defined at offset, optionally including
		the 'default' attributes.
	**/
	public function getAttributeRun(offset:Dynamic, ?includeDefaults:Dynamic):Dynamic;
	/**
		Get the string value of a named attribute at a given offset,
		if defined. 
		@param : offset
		the offset of the character for which the attribute run is to
		be obtained. 
		@param : attributeName
		the name of the attribute for which the value is to be returned,
		if defined. 
		@param : startOffset
		back-filled with the offset of the first character in the attribute
		run containing the character at offset. 
		@param : endOffset
		back-filled with the offset of the first character past the end
		of the attribute run containing the character at offset. 
		@param : defined
		back-filled with True if the attributeName has a defined value
		at offset, False otherwise. 
		@return the value of attribute (name-value pair) corresponding
		to "name", if defined.
	**/
	public function getAttributeValue(offset:Dynamic, attributeName:Dynamic):Dynamic;
	/**
		getAttributes is deprecated in favor of getAttributeRun. 
		@return the attributes at offset, as a semicolon-delimited set
		of colon-delimited name-value pairs.
	**/
	public function getAttributes(offset:Dynamic):Dynamic;
	/**
		Return the text content within a bounding box, as a list of Range
		structures. Depending on the TEXT_CLIP_TYPE parameters, glyphs
		which are clipped by the bounding box (i.e. which lie partially
		inside and partially outside it) may or may not be included in
		the ranges returned. 
		@param : x
		the minimum x ( i.e. leftmost) coordinate of the bounding box.
		@param : y
		the minimum y coordinate of the bounding box. 
		@param : width
		the horizontal size of the bounding box. The rightmost bound
		of the bounding box is (x + width); 
		@param : height
		the vertical size of the bounding box. The maximum y value of
		the bounding box is (y + height); 
		@param : coordType
		If 0, the above coordinates are interpreted as pixels relative
		to corner of the screen; if 1, the coordinates are interpreted
		as pixels relative to the corner of the containing toplevel window.
		@param : xClipType
		determines whether text which intersects the bounding box in
		the x direction is included. 
		@param : yClipType
		determines whether text which intersects the bounding box in
		the y direction is included.
	**/
	public function getBoundedRanges(x:Dynamic, y:Dynamic, width:Dynamic, height:Dynamic, coordType:Dynamic, xClipType:Dynamic, yClipType:Dynamic):Dynamic;
	/**
		@param : offset
		position
		@return an unsigned long integer whose value corresponds to the
		UCS-4 representation of the character at the specified text offset,
		or 0 if offset is out of range.
	**/
	public function getCharacterAtOffset(offset:Dynamic):Dynamic;
	/**
		Obtain a the bounding box, as x, y, width, and height, of the
		character or glyph at a particular character offset in this object's
		text content. The coordinate system in which the results are
		reported is specified by coordType. If an onscreen glyph corresponds
		to multiple character offsets, for instance if the glyph is a
		ligature, the bounding box reported will include the entire glyph
		and therefore may apply to more than one character offset. 
		The returned values are meaningful only if the Text has
		both STATE_VISIBLE and STATE_SHOWING.
		@param : offset
		the character offset of the character or glyph being queried.
		@param : coordType
		If 0, the results will be reported in screen coordinates, i.e.
		in pixels relative to the upper-left corner of the screen, with
		the x axis pointing right and the y axis pointing down. If 1,
		the results will be reported relative to the containing toplevel
		window, with the x axis pointing right and the y axis pointing
		down.
	**/
	public function getCharacterExtents(offset:Dynamic, coordType:Dynamic):Dynamic;
	/**
		Return an AttributeSet containing the text attributes which apply
		to all text in the object by virtue of the default settings of
		the document, view, or user agent; e.g. those attributes which
		are implied rather than explicitly applied to the text object.
		For instance, an object whose entire text content has been explicitly
		marked as 'bold' will report the 'bold' attribute via getAttributeRun(),
		whereas an object whose text weight is inspecified may report
		the default or implied text weight in the default AttributeSet.
	**/
	public function getDefaultAttributeSet():Dynamic;
	/**
		Deprecated in favor of getDefaultAttributeSet. 
		@return the attributes which apply to the entire text content,
		but which were not explicitly specified by the content creator.
	**/
	public function getDefaultAttributes():Dynamic;
	/**
		Obtain the number of separate, contiguous selections in the current
		Text object. Text objects which do not implement selection of
		discontiguous text regions will always return '0' or '1'. Note
		that "contiguous" is defined by continuity of the offsets, i.e.
		a text 'selection' is defined by a start/end offset pair. In
		the case of bidirectional text, this means that a continguous
		selection may appear visually discontiguous, and vice-versa.
		@return the number of contiguous selections in the current Text
		object.
	**/
	public function getNSelections():Dynamic;
	/**
		Get the offset of the character at a given onscreen coordinate.
		The coordinate system used to interpret x and y is determined
		by parameter coordType. 
		@param : x
		@param : y
		@param : coordType
		if 0, the input coordinates are interpreted relative to the entire
		screen, if 1, they are relative to the toplevel window containing
		this Text object. 
		@return the text offset (as an offset into the character array)
		of the glyph whose onscreen bounds contain the point x,y, or
		-1 if the point is outside the bounds of any glyph.
	**/
	public function getOffsetAtPoint(x:Dynamic, y:Dynamic, coordType:Dynamic):Dynamic;
	/**
		Obtain the bounding box which entirely contains a given text
		range. Negative values may be returned for the bounding box parameters
		in the event that all or part of the text range is offscreen
		or not mapped to the screen. 
		The returned values are meaningful only if the Text has
		both STATE_VISIBLE and STATE_SHOWING.
		@param : startOffset
		the offset of the first character in the specified range. 
		@param : endOffset
		the offset of the character immediately after the last character
		in the specified range. 
		@param : x
		an integer parameter which is back-filled with the minimum horizontal
		coordinate of the resulting bounding box. 
		@param : y
		an integer parameter which is back-filled with the minimum vertical
		coordinate of the resulting bounding box. 
		@param : width
		an integer parameter which is back-filled with the horizontal
		extent of the bounding box. 
		@param : height
		an integer parameter which is back-filled with the vertical extent
		of the bounding box. 
		@param : coordType
		If 0, the above coordinates are reported in pixels relative to
		corner of the screen; if 1, the coordinates are reported relative
		to the corner of the containing toplevel window.
	**/
	public function getRangeExtents(startOffset:Dynamic, endOffset:Dynamic, coordType:Dynamic):Dynamic;
	/**
		The result of calling getSelection with an out-of-range selectionNum
		(i.e. for a selection which does not exist) is not strictly defined,
		but should set endOffset equal to startOffset.
		@param : selectionNum
		indicates which of a set of non-contiguous selections to modify.
		@param : startOffset
		back-filled with the starting offset of the resulting substring,
		if one exists. 
		@param : endOffset
		back-filled with the offset of the character immediately following
		the resulting substring, if one exists. 
	**/
	public function getSelection(selectionNum:Dynamic):Dynamic;
	/**
		Obtain a subset of the text content of an object which includes
		the specified offset, delimited by character, word, line, sentence
		or paragraph granularity as specified by type. The starting and ending
		offsets of the resulting substring are returned in startOffset
		and endOffset.
		@param : offset
		the offset from which the substring search begins, and which
		must lie within the returned substring.
		@param : type
		the text granularity which determines whether the  returned text
		constitures a character, word, line, sentence or paragraph (and
		possibly attendant  whitespace). For all of those cases, boundaries
		will always be defined from the start of the current substring to
		the start of the following one for the same granularity.
		@param : startOffset
		back-filled with the starting offset of the resulting substring,
		if one exists.
		@param : endOffset
		back-filled with the offset of the character immediately following
		the resulting substring, if one exists.
		@return a string which is a substring of the text content of
		the object, delimited by the specified text granularity.
	**/
	public function getStringAtOffset(offset:Dynamic, type:Dynamic):Dynamic;
	/**
		Obtain all or part of the onscreen textual content of a Text
		object. If endOffset is specified as "-1", then this method will
		return the entire onscreen textual contents of the Text object.
		@param : startOffset
		back-filled with the starting offset of the resulting substring,
		if one exists. 
		@param : endOffset
		back-filled with the offset of the character immediately following
		the resulting substring, if one exists. 
		@return the textual content of the current Text object beginning
		startOffset (inclusive) up to but not including the character
		at endOffset.
	**/
	public function getText(startOffset:Int, endOffset:Int):Dynamic;
	/**
		Deprecated in favor of getStringAtOffset.
		Obtain a subset of the text content of an object which entirely
		follows offset, delimited by character, word, line, or sentence
		boundaries as specified by type. The starting and ending offsets
		of the resulting substring are returned in startOffset and endOffset.
		By definition, if such a substring exists, startOffset must be
		greater than offset. 
		@param : offset
		the offset from which the substring search begins, and which
		must lie before the returned substring. 
		@param : type
		the text-boundary delimiter which determines whether the returned
		text constitures a character, word, line, or sentence (and possibly
		attendant whitespace), and whether the start or ending of such
		a substring forms the boundary condition. 
		@param : startOffset
		back-filled with the starting offset of the resulting substring,
		if one exists. 
		@param : endOffset
		back-filled with the offset of the character immediately following
		the resulting substring, if one exists. 
		@return a string which is a substring of the text content of
		the object, delimited by the specified boundary condition.
	**/
	public function getTextAfterOffset(offset:Dynamic, type:Dynamic):Dynamic;
	/**
		Deprecated in favor of getStringAtOffset.
		Obtain a subset of the text content of an object which includes
		the specified offset, delimited by character, word, line, or
		sentence boundaries as specified by type. The starting and ending
		offsets of the resulting substring are returned in startOffset
		and endOffset. 
		@param : offset
		the offset from which the substring search begins, and which
		must lie within the returned substring. 
		@param : type
		the text-boundary delimiter which determines whether the returned
		text constitures a character, word, line, or sentence (and possibly
		attendant whitespace), and whether the start or ending of such
		a substring forms the boundary condition. 
		@param : startOffset
		back-filled with the starting offset of the resulting substring,
		if one exists. 
		@param : endOffset
		back-filled with the offset of the character immediately following
		the resulting substring, if one exists. 
		@return a string which is a substring of the text content of
		the object, delimited by the specified boundary condition.
	**/
	public function getTextAtOffset(offset:Dynamic, type:Dynamic):Dynamic;
	/**
		Deprecated in favor of getStringAtOffset.
		Obtain a subset of the text content of an object which entirely
		precedes offset, delimited by character, word, line, or sentence
		boundaries as specified by type. The starting and ending offsets
		of the resulting substring are returned in startOffset and endOffset.
		By definition, if such a substring exists, endOffset is less
		than or equal to offset. 
		@param : offset
		the offset from which the substring search begins. 
		@param : type
		the text-boundary delimiter which determines whether the returned
		text constitures a character, word, line, or sentence (and possibly
		attendant whitespace), and whether the start or ending of such
		a substring forms the boundary condition. 
		@param : startOffset
		back-filled with the starting offset of the resulting substring,
		if one exists. 
		@param : endOffset
		back-filled with the offset of the character immediately following
		the resulting substring, if one exists. 
		@return a string which is a substring of the text content of
		the object, delimited by the specified boundary condition.
	**/
	public function getTextBeforeOffset(offset:Dynamic, type:Dynamic):Dynamic;
	public function get_caretOffset():Dynamic;
	public function get_characterCount():Dynamic;
	/**
		Insert new text contents into an existing text object at a given
		location, while retaining the old contents. 
		@param : position
		the character offset into the Text implementor's content at which
		the new content will be inserted. 
		@param : text
		a UTF-8 string of which length characters will be inserted into
		the text object's text buffer. 
		@param : length
		the number of characters of text to insert. If the character
		count of text is less than or equal to length, the entire contents
		of text will be inserted.
		@return True if the text content was successfully inserted, False
		otherwise.
	**/
	public function insertText(position:Dynamic, text:Dynamic, length:Dynamic):Dynamic;
	/**
		Copy the text contents of the system clipboard, if any, into
		a Text object, inserting it at a particular character offset.
		@param : position
		the character offset before which the text will be inserted.
		@return True if the text was successfully pasted into the Text
		object, False otherwise.
	**/
	public function pasteText(position:Dynamic):Dynamic;
	static public function queryAction(x:Dynamic):Dynamic;
	static public function queryCollection(x:Dynamic):Dynamic;
	static public function queryComponent(x:Dynamic):Dynamic;
	static public function queryDocument(x:Dynamic):Dynamic;
	static public function queryEditableText(x:Dynamic):Dynamic;
	static public function queryHyperlink(x:Dynamic):Dynamic;
	static public function queryHypertext(x:Dynamic):Dynamic;
	static public function queryImage(x:Dynamic):Dynamic;
	static public function querySelection(x:Dynamic):Dynamic;
	static public function queryTable(x:Dynamic):Dynamic;
	static public function queryTableCell(x:Dynamic):Dynamic;
	static public function queryText(x:Dynamic):Dynamic;
	static public function queryValue(x:Dynamic):Dynamic;
	/**
		Deselect the text contained in the specified selectionNum, if
		such a selection exists, otherwise do nothing. Removal of a non-existant
		selectionNum has no effect. 
		@param : selectionNum
		indicates which of a set of non-contiguous selections to modify.
		@return True if the selection was successfully removed, False
		otherwise.
	**/
	public function removeSelection(selectionNum:Dynamic):Dynamic;
	/**
		Makes the text range visible on the screen at a given position by
		scrolling all necessary parents.
		@return True if scrolling was successful.
	**/
	public function scrollSubstringTo(startOffset:Dynamic, endOffset:Dynamic, scroll_type:Dynamic):Dynamic;
	/**
		Makes the text range visible on the screen at a given position by
		scrolling all necessary parents.
		@return True if scrolling was successful.
	**/
	public function scrollSubstringToPoint(startOffset:Dynamic, endOffset:Dynamic, coord_type:Dynamic, x:Dynamic, y:Dynamic):Dynamic;
	/**
		Programmatically move the text caret (visible or virtual, as
		above) to a given position. 
		@param : offset
		a long int indicating the desired character offset. Not all implementations
		of Text will honor setCaretOffset requests, so the return value
		below should be checked by the client. 
		@return TRUE if the request was carried out, or FALSE if the
		caret could not be moved to the requested position.
	**/
	public function setCaretOffset(offset:Dynamic):Dynamic;
	/**
		Modify an existing selection's start or ending offset.
		Calling setSelection for a selectionNum that is not already defined
		has no effect. The result of calling setSelection with a selectionNum
		greater than 0 for objects that do not include STATE_MULTISELECTABLE
		is undefined. 
		@param : selectionNum
		indicates which of a set of non-contiguous selections to modify.
		@param : startOffset
		the new starting offset for the selection 
		@param : endOffset
		the new ending offset for the selection 
		@return True if the selection corresponding to selectionNum is
		successfully modified, False otherwise.
	**/
	public function setSelection(selectionNum:Dynamic, startOffset:Dynamic, endOffset:Dynamic):Dynamic;
	/**
		Replace the text contents with a new string, discarding the old
		contents.
		@param : newContents
		a UTF-8 string with which the text object's contents will be
		replaced. 
		@return True if the text content was successfully changed, False
		otherwise.
	**/
	public function setTextContents(contents:Dynamic):Dynamic;
}