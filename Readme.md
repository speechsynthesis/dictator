

This is a basic STT App for Linux X11.

## Building Prerequisites

- haxe installed

For C++
 - vosk libraries https://github.com/alphacep/vosk-api/releases/tag/v0.3.45
( you can copy the .so in the build folder)

( You can also copy the file from libs/libvosk.so in this github repository)

## How to build

install libraries

```sh
    haxelib install haxe.hxml
```

Then build

```sh
    haxe haxe.hxml
```

## Running prerequisites

- a keyboard simulator : 
    xdotool : xdotool works better for now
    ydotool : WIP

- a screen overlay : only dzen2 for now

- a vosk voice : downloaded from here https://alphacephei.com/vosk/models



### C++ version


### python version

To have all the features, enable accessibility on the apps

For qt apps
```
export QT_ACCESSIBILITY=1
export QT_LINUX_ACCESSIBILITY_ALWAYS_ON=1

```

you need python3-pyatspi  ( install by apt)
 pip3 install vosk


## How to run

You need to have a config.ini in the same folder


The config ini will show where to find the files.

```ini
[fr]
model=modelFR
commandsfile=locale.csv
grammar=grammalecte

[en]
model=modelEN
commandsfile=localeEN.csv
```

For example here, if you do  `./dictator en`, it will load the model in the modelEN folder ( it can be a relative or an absolute path)
It will load the file of commands localeEN.csv

How to build a file of commands?


here is an example of a file. It is a CSV.
```csv
general;stop;stop
general;help;aide
general;hide;cacher
mode;stopListening;arrête d'écouter;
mode;write;écrire
mode;startListening;debout
mode;writeHere;ici
mode;key:Return;entrée
mode;navigation;naviguer
mode;editor;édition
mode;spell;épeler
mode;quit;quitter
mode;stopListening;arrete
mode;stopListening;arrête
mode;stopListening;silence;
deaf;startListening;écoute
dictation;key:Return;entrée
dictation;deleteLastWord;efface
dictation;stop;stop
dictation;capitalize;majuscule
dictation;word:,;virgule
dictation;word:.;point
dictation;word:?;point d'interrogation
dictation;word:!;point d'exclamation
dictation;word::);sourire
dictation;key:Return;entrée
dictation;spell;épeler
editor;goAfterSpecificWord;après
editor;goWordStart;début mot
editor;goWordEnd;fin mot
editor;goSentenceStart;début phrase
editor;goSentenceEnd;fin phrase
editor;goLineStart;début ligne
editor;goLineEnd;fin ligne
editor;selectWord;sélectionner mot
editor;selectSentence;sélectionner phrase
editor;selectLigne;sélectionner ligne
```

### C++ version

you can have vosk_api.so in the usual paths for libs ("/usr/lib/", etc) or  inside the folder of the app.

```sh
cp ./libs/vosk/libs/libvosk.so build/speech/
```
The app will be build in `build/speech/`


Then you can do
```sh
cd build/speech
./dictator en
```

### python version
```python 
python3 dictator.py en
```

## Known Commandes

Here are the commands

```
general:
	cancelCommand;     // Cancel what was said
	stop;              // Go back to choosing Modes
	hide;              // hide overlat
	help;              // show overlay
	key(keys:String);  // Presses keys

mode:
	stopListening;
	writeHere;
	write;
	spell;
	#if python
	navigation;
	editor;
	#end
	quit; // exit : mode none

deaf:
    startListening; // Starts listening

dictation:
	capitalize; // Capitalizes
	spell; // Change to spell mode
	deleteLastWord; // Ctrl back   ( ctrl w on vim)
	key(keys:String);
	word(word:String);

editor:
	goAfterSpecificWord;
	goWordStart;
	goWordEnd;
	goSentenceStart;
	goSentenceEnd;
	goLineStart;
	goLineEnd;
	selectWord;
	selectSentence;
	selectLine;
```

There are a special type of commands like the *word*  "dictation;word:?;interrogation mark"
For example, it will type the word "?"  when you say interrogation mark

There is also the *key* where you can "press keys"  "dictation;key:Return;enter"
You can see the list of keys here
https://gitlab.com/cunidev/gestures/-/wikis/xdotool-list-of-key-codes


## What models to use


You can download a model here : https://alphacephei.com/vosk/models
*vosk-model-small-en-us-0.15*
try to see if it's understand you well

*vosk-model-en-us-0.22*
*vosk-model-en-us-0.22-lgraph*

There are bugs with these specific models https://github.com/alphacep/vosk-api/issues/837

*vosk-model-en-us-0.42-gigaspeech*
If you have lots or ram and a fast computer

*vosk-model-en-us-0.21*
It works great for me

vosk-model-fr-0.6-linto-2.2.0
works quite well, uses some ram
vosk-model-small-fr-0.22
doesn't fit my voice



## What's the difference with other transcribers ?

There is numen https://git.sr.ht/~geb/numen and nerd dictation  https://github.com/ideasman42/nerd-dictation

There are few originals ideas here:
    - an overlay  can show you the commands, so you don't have to remember everyting at once
    - in French, you can use some grammatical corrector 
    - you can write in an program that is not focused ( you can type at the same time elsewere) only on X11 and doesn't work for all apps.
    - you can navigate (only push buttons for now ) on apps that supports accessibility
    - you have more edition commands on apps that supports accessibility



## Future fast improvements
- more commands
- some bugs to correct
- more atspi implementation

## Possible future improvements

- more commands
- commands working differently depending on the app ( in vim and terminal deleting words uses different shortcuts)


## Technical problems 

Sometimes, when the app is closed the generated overlay isn't killed   ( do pkill dzen2)